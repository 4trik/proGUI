/**
 * @param {string} type - The generic type of resource to add the gatherer for.
 */
Player.prototype.AddResourceGatherer = function (type, entID) {
    if (type === undefined)
        return;
    //const cmpGuiInterface = Engine.QueryInterface(1, IID_GuiInterface);
    //cmpGuiInterface.addResourceGatherer(this.playerID, type, entID);
    ++this.resourceGatherers[type];
};

/**
 * @param {string} type - The generic type of resource to remove the gatherer from.
 */
Player.prototype.RemoveResourceGatherer = function (type, entID) {
    //const cmpGuiInterface = Engine.QueryInterface(1, IID_GuiInterface);
    //cmpGuiInterface.removeResourceGatherer(this.playerID, type, entID);
    --this.resourceGatherers[type];
};

