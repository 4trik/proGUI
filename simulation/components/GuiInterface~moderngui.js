const moderngui_excluded_techs = [
	"civbonuses",
	"pair",
	"phase",
	"soldier_ranged_experience",
	"unit_advanced",
	"unit_elephant_african",
	"unit_elephant_indian",
	"unit_elite",
	"upgrade_rank_advanced_mercenary"
];
const moderngui_excluded_structure_killed = [
	"Palisade"
];

const moderngui_template_keys = {
	"structures/palisades_tower": "structures/palisades_tower",
	"structures/palisades_medium": "structures/palisades_tower",
	"structures/palisades_long": "structures/palisades_tower",
	"structures/palisades_gate": "structures/palisades_tower"
};

const moderngui_more_military_techs = [
	"archer_attack_spread",
	"archery_tradition",
	"attack_soldiers_will",
	"poison_arrows",
	"poison_blades"
];

const moderngui_resources_techs = {
	"food": [
		"gather_wicker_baskets",
		"gather_ahimsa",
		"gather_farming_plows",
		"gather_farming_seed_drill",
		"gather_farming_water_weeding",
		"gather_farming_chain_pump",
		"gather_farming_harvester",
		"gather_farming_training",
		"gather_farming_fertilizer",
		"gather_animals_stockbreeding",
		"gather_capacity_basket",
		"gather_capacity_wheelbarrow",
		"gather_capacity_carts"
	],
	"wood": [
		"gather_lumbering_ironaxes",
		"gather_lumbering_sharpaxes",
		"gather_lumbering_strongeraxes",
		"gather_capacity_basket",
		"gather_capacity_wheelbarrow",
		"gather_capacity_carts"
	],
	"stone": [
		"gather_mining_servants",
		"gather_mining_serfs",
		"gather_mining_slaves",
		"gather_capacity_basket",
		"gather_capacity_wheelbarrow",
		"gather_capacity_carts"
	],
	"metal": [
		"gather_mining_wedgemallet",
		"gather_mining_shaftmining",
		"gather_mining_silvermining",
		"gather_capacity_basket",
		"gather_capacity_wheelbarrow",
		"gather_capacity_carts"
	]
};

const moderngui_resources_types = Object.keys(moderngui_resources_techs);

const moderngui_phases = ["imperial", "city", "town", "village"];

const moderngui_buildings = [ //Will inherhit the first found mode, so order matters.
	{ "mode": "Corral", "classes": ["Corral"] },
	{ "mode": "Market", "classes": ["Market"] },
	{ "mode": "Researcher", "classes": ["Economic", "Forge"] },
	{ "mode": "Trainer", "classes": ["Military", "Civic", "Council", "Palace", "Market", "Syssiton", "Gymnasium", "ArmyCamp"] } //TODO reintroduce the option to choose what entities are eligible to the trainer.
];
const moderngui_building_types = [
	{ "mode": "civic_buildings", "classes": ["House"] },
	{ "mode": "military_buildings", "classes": ["Military", "Syssiton", "Council", "Gymnasium", "Civic", "Dock", "Corral"] },
	{ "mode": "economic_buildings", "classes": ["Economic", "Resource"] },
	{ "mode": "defensive_buildings", "classes": ["Defensive", "Palisade", "Wall"] }
];

function splitRatingFromNick(playerName) {
	const result = /^(\S+) \((\d+)\)$/g.exec(playerName);
	const nick = (result ? result[1] : playerName).trim();
	const rating = result ? result[2] : "";
	return { nick, rating };
}

class CustomQueue extends Map {
	static RegexRank = /_[ae]$/;
	static RegexHouse = /^(units\/.+)_house$/;
	static RegexStructures = /^(structures\/)(.+\/)/;

	add({ mode, templateType, entity, template, count, progress, classesList }) {

		template = moderngui_template_keys[template] ?? template;
		template = template
			.replace(CustomQueue.RegexRank, "_b")
			.replace(CustomQueue.RegexHouse, "$1");
		const key = `${mode}:${template.replace(CustomQueue.RegexStructures, "$1")}`;

		const obj = this.get(key);
		if (obj) {
			obj.count += count;
			obj.progress += progress;
			if (entity != null) obj.entity.push(entity);
		}
		else {
			this.set(key, {
				mode,
				count,
				template,
				progress,
				"entity": entity != null ? [entity] : [],
				templateType,
				classesList
			});
		}
	}

	toArray() {
		return Array.from(this.values());
	}
}

const moderngui_players_weakmap = new WeakMap();
const moderngui_fullupdate_interval = 1200;
let moderngui_fullupdate_last = 0;

GuiInterface.prototype.GetPlayerEntities = function (player, playerID) {
	if (playerID?.playerID == undefined)
		return Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager).GetEntitiesByPlayer(player);
	else
		return Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager).GetEntitiesByPlayer(playerID.playerID);
};

// let moderngui_units_kill_counts = {};
// let moderngui_last_kill_or_death_position = {};
// /**
//  * Optimized stats function for boonGUI stats overlay
//  * @param {number} attackerID - The entityID of the attacker.
//  * @param {number} killedID - The entityID of the killed unit.
//  * @param {object} loot - The loot value of the killed unit.
//  * @param {number} xp - The XP gained from the killed unit.
//  */
// GuiInterface.prototype.unitKilled = function (attackerID, killedID, loot, xp) {
// 	Engine.ProfileStart("ProGUIStats:update:UnitKillCalc");

// 	const cmpOwnership = Engine.QueryInterface(killedID, IID_Ownership);
// 	const cmpOwnershipAttacker = Engine.QueryInterface(attackerID, IID_Ownership);
// 	const positionAttacker = Engine.QueryInterface(attackerID, IID_Position)?.GetPosition2D() || { x: 0, y: 0 };
// 	const positionKilled = Engine.QueryInterface(killedID, IID_Position)?.GetPosition2D() || { x: 0, y: 0 };

// 	if (!cmpOwnership || !cmpOwnershipAttacker) return;

// 	const killedOwnerID = cmpOwnership.GetOwner();
// 	const attackerOwnerID = cmpOwnershipAttacker.GetOwner();

// 	const classesList = Engine.QueryInterface(killedID, IID_Identity)?.classesList;
// 	if (classesList?.includes("Fundation") || classesList?.some(className => moderngui_excluded_structure_killed.includes(className))) return;

// 	if (!moderngui_units_kill_counts[attackerID]) {
// 		moderngui_units_kill_counts[attackerID] = {
// 			gaia: 0,
// 			player: 0,
// 			structure: 0,
// 			loot: {},
// 			xp: 0
// 		};
// 	}

// 	if (loot) {
// 		for (const resource in loot) {
// 			moderngui_units_kill_counts[attackerID].loot[resource] = (moderngui_units_kill_counts[attackerID].loot[resource] || 0) + loot[resource];
// 		}
// 	}

// 	if (xp) {
// 		moderngui_units_kill_counts[attackerID].loot.xp = (moderngui_units_kill_counts[attackerID].loot.xp || 0) + xp;
// 	}

// 	if (classesList?.includes("Structure")) {
// 		moderngui_units_kill_counts[attackerID].structure++;
// 	} else {
// 		switch (killedOwnerID) {
// 			case 0:
// 				moderngui_units_kill_counts[attackerID].gaia++;
// 				break;
// 			case attackerOwnerID:
// 				break;
// 			default:
// 				moderngui_units_kill_counts[attackerID].player++;
// 				break;
// 		}
// 	}

// 	switch (killedOwnerID) {
// 		case 0:
// 			break;
// 		case attackerOwnerID:
// 			moderngui_last_kill_or_death_position[killedOwnerID] = positionKilled;
// 			break;
// 		default:
// 			moderngui_last_kill_or_death_position[attackerOwnerID] = positionAttacker;
// 			moderngui_last_kill_or_death_position[killedOwnerID] = positionKilled;
// 			break;
// 	}

// 	Engine.ProfileStop();
// };


// /**
//  * Get the kill count for a given entity
//  * @param {number} PlayerID - Player ID
//  * @returns {object} The position where the fight occured
//  */
// GuiInterface.prototype.getPlayerLastFightPosition = function (id, PlayerID) {
// 	return moderngui_last_kill_or_death_position[PlayerID.id] || null;
// };
// /**
//  * Get the kill count for a given entity
//  * @param {number} entityID - The entityID of the entity.
//  * @returns {number} The kill count for the entity.
//  */
// GuiInterface.prototype.getPlayerKillCount = function (id, entityID) {
// 	return moderngui_units_kill_counts[entityID.entityID]?.player || 0;
// };
// /**
//  * Get the kill count for a given entity
//  * @param {number} entityID - The entityID of the entity.
//  * @returns {number} The kill count for the entity.
//  */
// GuiInterface.prototype.getGaiaKillCount = function (id, entityID) {
// 	return moderngui_units_kill_counts[entityID.entityID]?.gaia || 0;
// };
// /**
//  * Get the structure kill count for a given entity
//  * @param {number} entityID - The entityID of the entity.
//  * @returns {number} The kill count for the entity.
//  */
// GuiInterface.prototype.getStructureDestroyedCount = function (id, entityID) {
// 	return moderngui_units_kill_counts[entityID.entityID]?.structure || 0;
// };
// /**
//  * Get the total looted for a given entity
//  * @param {number} entityID - The entityID of the entity.
//  * @returns {number} The total loot for the entity.
//  */
// GuiInterface.prototype.getUnitTotalLooted = function (id, entityID) {
// 	return moderngui_units_kill_counts[entityID.entityID]?.loot || {};
// };

// let moderngui_resourceGatherer = {};

// /**
//  * Adds a resource gatherer entity to the moderngui_resourceGatherer object.
//  * @param {string} playerID - The ID of the player who owns the resource gatherer.
//  * @param {string} type - The type of resource the gatherer collects.
//  * @param {string} entityID - The ID of the resource gatherer entity.
//  */
// GuiInterface.prototype.addResourceGatherer = function (playerID, type, entityID) {
// 	if (!moderngui_resourceGatherer[playerID]) {
// 		moderngui_resourceGatherer[playerID] = {};
// 	}

// 	if (!moderngui_resourceGatherer[playerID][type]) {
// 		moderngui_resourceGatherer[playerID][type] = [];
// 	}

// 	moderngui_resourceGatherer[playerID][type].push(entityID);
// }
// /**
//  * Removes a resource gatherer entity from the moderngui_resourceGatherer object.
//  * @param {string} playerID - The ID of the player who owns the resource gatherer.
//  * @param {string} type - The type of resource the gatherer collects.
//  * @param {string} entityID - The ID of the resource gatherer entity to remove.
//  */
// GuiInterface.prototype.removeResourceGatherer = function (playerID, type, entityID) {
// 	if (moderngui_resourceGatherer[playerID] && moderngui_resourceGatherer[playerID][type]) {
// 		const index = moderngui_resourceGatherer[playerID][type].indexOf(entityID);
// 		if (index !== -1) {
// 			moderngui_resourceGatherer[playerID][type].splice(index, 1);
// 		}
// 	}
// }
GuiInterface.prototype.getResourceGathererList = function (id, filters) {
	return moderngui_resourceGatherer[filters.playerID]?.[filters.type] || [];
};

/**
 * Opimitzed stats function for boonGUI stats overlay
*/

GuiInterface.prototype.moderngui_GetOverlay = function (_, { g_IsObserver, g_ViewedPlayer, g_LastTickTime }) {
	const ret = {
		"players": []
	};

	let isFullUpdate = false;
	if (g_LastTickTime - moderngui_fullupdate_last >= moderngui_fullupdate_interval) {
		isFullUpdate = true;
		moderngui_fullupdate_last = g_LastTickTime;
	}

	const cmpPlayerManager = Engine.QueryInterface(SYSTEM_ENTITY, IID_PlayerManager);
	const cmpRangeManager = Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager);
	const cmpTemplateManager = Engine.QueryInterface(SYSTEM_ENTITY, IID_TemplateManager);
	const numPlayers = cmpPlayerManager.GetNumPlayers();

	const cmpPlayers = [];
	for (let index = 0; index < numPlayers; ++index) {
		const playerEnt = cmpPlayerManager.GetPlayerByID(index);
		const cmpPlayer = QueryPlayerIDInterface(index);
		const cmpIdentity = Engine.QueryInterface(playerEnt, IID_Identity);
		const cmpDiplomacy = Engine.QueryInterface(playerEnt, IID_Diplomacy);
		const state = cmpPlayer.GetState();
		const hasSharedLos = cmpDiplomacy.HasSharedLos();
		cmpPlayers.push({ index, state, hasSharedLos, cmpPlayer, cmpIdentity, cmpDiplomacy });
	}

	const cmpPlayerViewed = cmpPlayers[g_ViewedPlayer];
	ret.players = cmpPlayers.filter(({ index, state, cmpPlayer, cmpDiplomacy }) => {
		if (index === 0)
			return false; // Gaia index 0
		if (state == "defeated" && index != g_ViewedPlayer)
			return false;
		if (!cmpPlayerViewed || index == g_ViewedPlayer)
			return true;
		if (!cmpPlayerViewed.hasSharedLos || !cmpDiplomacy.IsMutualAlly(cmpPlayerViewed.index))
			return false;
		return true;
	}).map(({ index, cmpPlayer, cmpIdentity, cmpDiplomacy, state, hasSharedLos }) => {
		const cmpTechnologyManager = QueryPlayerIDInterface(index, IID_TechnologyManager);
		const cmpPlayerStatisticsTracker = QueryPlayerIDInterface(index, IID_StatisticsTracker);

		const player = {
			index,
			state,
			hasSharedLos,

			// @cmpPlayer
			"name": cmpIdentity.GetName(),
			"civ": cmpIdentity.GetCiv(),
			"team": cmpDiplomacy.GetTeam(),
			"trainingBlocked": cmpPlayer.IsTrainingBlocked(),
			"popCount": cmpPlayer.GetPopulationCount(),
			"popLimit": cmpPlayer.GetPopulationLimit(),
			"popMax": cmpPlayer.GetMaxPopulation(),
			"resourceCounts": cmpPlayer.GetResourceCounts(),
			"resourceGatherers": cmpPlayer.GetResourceGatherers(),

			// @cmpTechnologyManager
			"classCounts": cmpTechnologyManager?.GetClassCounts() ?? {}
		};

		let cached = moderngui_players_weakmap.get(cmpPlayer);
		let updateCache = isFullUpdate;

		if (!cached) {
			cached = {};
			updateCache = true;
			moderngui_players_weakmap.set(cmpPlayer, cached);
		}

		if (updateCache) {
			cached.civCentres = [];
			cached.queue = new CustomQueue();
			cached.production_buildings = new CustomQueue();
			cached.resourcesGathered = cmpPlayerStatisticsTracker?.resourcesGathered || {};
			cached.enemyUnitsKilledTotal = cmpPlayerStatisticsTracker?.enemyUnitsKilled.Unit ?? 0;
			cached.enemyUnitsKilledTotalValue = cmpPlayerStatisticsTracker?.enemyUnitsKilledValue ?? 0;
			cached.unitsLostTotal = cmpPlayerStatisticsTracker?.unitsLost.Unit ?? 0;
			cached.unitsLostTotalValue = cmpPlayerStatisticsTracker?.unitsLostValue ?? 0;
		}

		// (1) Get Nickname and Rating
		const { nick, rating } = splitRatingFromNick(player.name);
		player.nick = nick;
		player.rating = rating;

		// (2) Get Phase
		let phase = "";
		if (cmpTechnologyManager) {
			for (const _phase of moderngui_phases) {
				if (cmpTechnologyManager.IsTechnologyResearched(`phase_${_phase}`)) {
					phase = _phase;
					break;
				}
			}
		}
		player.phase = phase;

		// (3) Get Statistics
		player.resourcesGathered = cached.resourcesGathered;
		player.enemyUnitsKilledTotal = cached.enemyUnitsKilledTotal;
		player.enemyUnitsKilledTotalValue = cached.enemyUnitsKilledTotalValue;
		player.unitsLostTotal = cached.unitsLostTotal;
		player.unitsLostTotalValue = cached.unitsLostTotalValue;
		player.killDeathRatio = cached.enemyUnitsKilledTotal / cached.unitsLostTotal;
		player.killValueRatio = cached.enemyUnitsKilledTotalValue / cached.unitsLostTotalValue;

		// (5) Get Number of allies
		player.numberAllies = cmpDiplomacy.GetMutualAllies().filter(playerNumber => cmpPlayers[playerNumber].state != "defeated").length;

		// (6) Get Researched technologies set
		player.researchedTechs = new Set(cmpTechnologyManager?.GetResearchedTechs() ?? []);

		// (7) Get StartedResearch technologies
		player.startedResearch = this.GetStartedResearch(index);

		// (8) Get Resources related technologies
		const resourcesTechs = {};
		for (const resType of moderngui_resources_types) {
			resourcesTechs[resType] = moderngui_resources_techs[resType].filter(tech =>
				cmpTechnologyManager?.IsTechnologyResearched(tech)
			);
		}
		player.resourcesTechs = resourcesTechs;

		// (9) Get Military and economy related technologies
		const militaryTechs = [];
		let militaryTechsCount = 0;
		let economyTechsCount = 0;
		for (const template of player.researchedTechs) {
			if (moderngui_excluded_techs.some(s => template.includes(s))) continue;
			let mode;
			if (template.startsWith("soldier_") || moderngui_more_military_techs.includes(template)) {
				militaryTechsCount++;
				militaryTechs.push(template);
				mode = "military_technologies";
			}
			else if (template.startsWith("gather_")) {
				economyTechsCount++;
				mode = "economy_technologies";
			}
			else {
				mode = "other_technologies";
			}

			if (updateCache) {
				cached.queue.add({ mode, "count": 1, template, "progress": 1, "entity": null, "templateType": "technology", "classesList": null });
			}
		}

		player.militaryTechsCount = militaryTechsCount;
		player.economyTechsCount = economyTechsCount;
		player.militaryTechs = militaryTechs;

		// (10) Get all entities
		if (updateCache) {
			for (const entity of cmpRangeManager.GetEntitiesByPlayer(index)) {
				const cmpProductionQueue = Engine.QueryInterface(entity, IID_ProductionQueue);
				const classesList = Engine.QueryInterface(entity, IID_Identity)?.classesList;
				if (classesList) {
					if (cmpProductionQueue) {
						if (classesList.includes("Structure")) {
							if (classesList.includes("CivCentre")) //Should only be used by TopPanel "focusCC", so fornow, valid if it's a fundation for nomad mode etc.
								cached.civCentres.push(entity);
							if (classesList.includes("Foundation"))
								continue;
							const template = cmpTemplateManager.GetCurrentTemplateName(entity);
							let templateType = "productionBuilding";
							let mode = null;
							for (const type of moderngui_buildings) {
								if (type.classes.some(c => classesList.includes(c))) {
									mode = type.mode;
									cached.production_buildings.add({ mode: `${mode}`, templateType, entity, template, "count": 1, "progress": 0, "classesList": classesList });
									break;
								}
							}
							templateType = "unit";
							mode = moderngui_building_types[0].mode;
							for (const type of moderngui_building_types) {
								if (type.classes.some(c => classesList.includes(c))) {
									mode = type.mode;
									break;
								}
							}
							if (cmpProductionQueue.queue.length === 0) {
								cached.queue.add({ mode: `${mode}_idle`, templateType, entity, template, "count": 1, "progress": 0, "classesList": classesList });
							}
							cached.queue.add({ mode, templateType, entity, template, "count": 1, "progress": 0, "classesList": classesList });
						}
					}
					if (classesList.includes("Unit") &&
						!classesList.includes("Relic") &&
						!classesList.includes("Domestic")) {
						const template = cmpTemplateManager.GetCurrentTemplateName(entity);
						const mode = "units";
						const templateType = "unit";
						cached.queue.add({ mode, templateType, entity, template, "count": 1, "progress": 0, "classesList": classesList });
					}
				}

				const cmpUnitAI = Engine.QueryInterface(entity, IID_UnitAI);
				if (cmpUnitAI?.isIdle && !cmpUnitAI.isGarrisoned) {
					const cmpTurretable = Engine.QueryInterface(entity, IID_Turretable);
					if (cmpTurretable?.IsEjectable())
						continue;
					//  keep in sync with g_boonGUI_WorkerTypes
					if ((classesList.includes("FemaleCitizen") ||
						classesList.includes("Trader") ||
						classesList.includes("FishingBoat") ||
						classesList.includes("CitizenSoldier")) &&
						!classesList.includes("Mercenary")) {
						const template = cmpTemplateManager.GetCurrentTemplateName(entity);
						const mode = "idle";
						const templateType = "unit";
						cached.queue.add({ mode, templateType, entity, template, "count": 1, "progress": 0, classesList });
					}
				}

				if (cmpProductionQueue) {
					for (const queue of cmpProductionQueue.queue) {
						if (!queue.started || queue.paused) continue;
						const cmpTrainer = Engine.QueryInterface(queue.producer, IID_Trainer);
						const cmpResearcher = Engine.QueryInterface(queue.producer, IID_Researcher);
						const mode = "production";
						if (queue.entity) {
							const { count, "progress": reverseProgress, "unitTemplate": template, neededSlots } = cmpTrainer.GetBatch(queue.entity);
							//  Limit units to actual production and not list training of an unit that is blocked.
							if (neededSlots > 0) continue;
							const progress = reverseProgress;
							const templateType = "unit";
							cached.queue.add({ mode, templateType, entity, template, count, progress, "classesList": null });
						}

						if (queue.technology) {
							const { "progress": reverseProgress, "templateName": template } = cmpResearcher.GetResearchingTechnology(queue.technology);
							const progress = reverseProgress;
							const templateType = "technology";
							const count = 1;
							cached.queue.add({ mode, templateType, entity, template, count, progress, "classesList": null });
						}
					}
				}

				const cmpFoundation = Engine.QueryInterface(entity, IID_Foundation);
				if (cmpFoundation?.committed) {
					const { hitpoints, maxHitpoints } = Engine.QueryInterface(entity, IID_Health);
					const mode = "production";
					const templateType = "unit";
					const count = 1;
					const template = cmpFoundation.finalTemplateName;
					const progress = (hitpoints / maxHitpoints);
					cached.queue.add({ mode, templateType, entity, template, count, progress, "classesList": null });
				}
			}
		}

		player.civCentres = cached.civCentres;
		player.queue = cached.queue.toArray();
		player.productionBuildings = cached.production_buildings.toArray();

		//warn(player.queue.length)
		//warn(player.productionBuildings.length)

		return player;
	});

	// If the view is set to the player, the line moves to the top.
	if (cmpPlayers[g_ViewedPlayer]) {
		const sortedViewedPlayerAtTop = function (a, b) {
			if ((a.name === cmpPlayerViewed.cmpIdentity.name) != (b.name === cmpPlayerViewed.cmpIdentity.name))
				return a.name === cmpPlayerViewed.cmpIdentity.name ? -1 : 1;
			return a.team - b.team;
		};
		ret.players.sort(sortedViewedPlayerAtTop);
	}
	ret.players.sort((a, b) => a.team - b.team);
	return ret;
};

GuiInterface.prototype.DisplayRallyPoint = function (player, cmd) {
	// if selection did not change (cmd.watch == true)
	// Limit updates to remote rally points

	if (cmd.watch && this.ChangedRallyPoints.size == 0) return;
	if (!cmd.watch) this.LocalRallyPoints.clear();

	const cmpPlayer = QueryPlayerIDInterface(player);
	const cmpPlayerManager = Engine.QueryInterface(SYSTEM_ENTITY, IID_PlayerManager);
	const playerEnt = cmpPlayerManager.GetPlayerByID(player);
	const cmpDiplomacy = Engine.QueryInterface(playerEnt, IID_Diplomacy);

	// If there are some rally points already displayed, first hide them.
	for (const ent of this.entsRallyPointsDisplayed) {
		const cmpRallyPointRenderer = Engine.QueryInterface(ent, IID_RallyPointRenderer);
		if (cmpRallyPointRenderer)
			cmpRallyPointRenderer.SetDisplayed(false);
	}

	this.entsRallyPointsDisplayed = [];

	// Show the rally points for the passed entities.
	for (const ent of cmd.entities) {
		const cmpRallyPointRenderer = Engine.QueryInterface(ent, IID_RallyPointRenderer);
		if (!cmpRallyPointRenderer)
			continue;

		// Entity must have a rally point component to display a rally point marker
		// (regardless of whether cmd specifies a custom location).
		const cmpRallyPoint = Engine.QueryInterface(ent, IID_RallyPoint);
		if (!cmpRallyPoint)
			continue;

		const cmpOwnership = Engine.QueryInterface(ent, IID_Ownership);

		// Rally point should be displayed if one of the following is true:
		// 1) It is owned by the player
		// 2) The player is an observer
		// 3) The player is a mutual ally with shared LOS

		if (cmpPlayer && cmpOwnership) {
			const owner = cmpOwnership.GetOwner();
			if (owner != player) {
				if (!cmpDiplomacy.IsMutualAlly(owner) || !cmpDiplomacy.HasSharedLos()) {
					continue;
				}
			}
		}

		// If the command was passed an explicit position, use that and
		// override the real rally point position; otherwise use the real position.
		let pos;
		if (cmd.x && cmd.z)
			pos = cmd;
		else
			// May return undefined if no rally point is set.
			pos = cmpRallyPoint.GetPositions()[0];

		if (pos) {
			// Update position on changes (cmd.queued is set).
			// Note that Add-/SetPosition take a CFixedVector2D which has X/Y components, not X/Z.
			if ("queued" in cmd) {
				if (cmd.queued == true)
					cmpRallyPointRenderer.AddPosition(new Vector2D(pos.x, pos.z));
				else
					cmpRallyPointRenderer.SetPosition(new Vector2D(pos.x, pos.z));

				this.LocalRallyPoints.add(ent);
			}
			else if (!cmpRallyPointRenderer.IsSet()) {
				// Rebuild the renderer when not set (when reading saved game or in case of building update).
				for (const posi of cmpRallyPoint.GetPositions())
					cmpRallyPointRenderer.AddPosition(new Vector2D(posi.x, posi.z));
			}
			else if (!this.LocalRallyPoints.has(ent) && this.ChangedRallyPoints.has(ent)) {
				cmpRallyPointRenderer.SetPosition(new Vector2D(pos.x, pos.z));
				for (const posi of cmpRallyPoint.GetPositions())
					cmpRallyPointRenderer.AddPosition(new Vector2D(posi.x, posi.z));
			}

			cmpRallyPointRenderer.SetDisplayed(true);

			// Remember which entities have their rally points displayed so we can hide them again.
			this.entsRallyPointsDisplayed.push(ent);
		}

		this.ChangedRallyPoints.clear();
	}
};

GuiInterface.prototype.LocalRallyPoints = new Set();
GuiInterface.prototype.ChangedRallyPoints = new Set();

const moderngui_exposedFunctions = {
	"moderngui_GetOverlay": 1,
	"GetPlayerEntities": 1,
	"getResourceGathererList": 1,
};

// Idea from @nani's autociv mod, makes it much easier to register additional GuiInterfaceCall functions.
GuiInterface.prototype.ScriptCall = new Proxy(GuiInterface.prototype.ScriptCall,
	{
		apply(target, that, args) {
			const [player, name, vargs] = args;
			if (name in moderngui_exposedFunctions)
				return that[name](player, vargs);

			return target.apply(that, args);
		}
	});

Engine.ReRegisterComponentType(IID_GuiInterface, "GuiInterface", GuiInterface);
