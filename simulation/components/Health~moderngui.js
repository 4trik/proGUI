
/**
 * Called when an entity kills us.
 * @param {number} attacker - The entityID of the killer.
 * @param {number} attackerOwner - The playerID of the attacker.
 */
Health.prototype.KilledBy = function (attacker, attackerOwner) {
	let cmpAttackerOwnership = Engine.QueryInterface(attacker, IID_Ownership);
	if (cmpAttackerOwnership) {
		let currentAttackerOwner = cmpAttackerOwnership.GetOwner();
		if (currentAttackerOwner != INVALID_PLAYER)
			attackerOwner = currentAttackerOwner;
	}

	// Add to killer statistics.
	let cmpKillerPlayerStatisticsTracker = QueryPlayerIDInterface(attackerOwner, IID_StatisticsTracker);
	if (cmpKillerPlayerStatisticsTracker)
		cmpKillerPlayerStatisticsTracker.KilledEntity(this.entity);

	// Add to loser statistics.
	let cmpTargetPlayerStatisticsTracker = QueryOwnerInterface(this.entity, IID_StatisticsTracker);
	if (cmpTargetPlayerStatisticsTracker)
		cmpTargetPlayerStatisticsTracker.LostEntity(this.entity);

	/*Engine.ProfileStart("ModernGUI:unitKillCounter"); //Sadly, remove this for performances
	//Increment kill count of the attacker for ModGUI
	let cmpLoot = Engine.QueryInterface(this.entity, IID_Loot);
	let cmpGuiInterface = Engine.QueryInterface(1, IID_GuiInterface);
	let loot = null;
	let xp = null;
	if (cmpLoot) {
		loot = cmpLoot.GetResources();
		xp = cmpLoot.GetXp();
	}

	cmpGuiInterface.unitKilled(attacker, this.entity, loot, xp);
	Engine.ProfileStop();*/
	let cmpLooter = Engine.QueryInterface(attacker, IID_Looter);

	if (cmpLooter) {
		cmpLooter.Collect(this.entity);
	}
};

/**
 * Handle what happens when the entity dies.
 */
Health.prototype.HandleDeath = function () {
	const cmpSoundManager = Engine.QueryInterface(SYSTEM_ENTITY, IID_SoundManager);
	const cmpDeathDamage = Engine.QueryInterface(this.entity, IID_DeathDamage);
	const cmpIdentity = Engine.QueryInterface(this.entity, IID_Identity);

	if (cmpDeathDamage)
		cmpDeathDamage.CauseDeathDamage();

	if (MatchesClassList(cmpIdentity.GetClassesList(), "Hero"))
		cmpSoundManager.PlaySoundGroup("actor/hero/death/coqui-ai_TTS_Hero_Death.xml", this.entity);
	PlaySound("death", this.entity);

	if (this.template.SpawnEntityOnDeath)
		this.CreateDeathSpawnedEntity();

	switch (this.template.DeathType) {
		case "corpse":
			this.CreateCorpse();
			break;

		case "remain":
			return;

		case "vanish":
			break;

		default:
			error(`Invalid template.DeathType: ${this.template.DeathType}`);
			break;
	}

	Engine.DestroyEntity(this.entity);
};

Engine.ReRegisterComponentType(IID_Health, "Health", Health);

