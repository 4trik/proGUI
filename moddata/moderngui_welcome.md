# ModernGUI
ModernGUI regroup a lot of graphic and interactive features to enhance your 0 A.D. playing experience.

This page will help choose between a selection of configuration for your new GUI.

## Quick Configuration
### EcoPanels

Add complementary panels that offer new ways to manage your production.
- Default: OFF.
- Caution: Features enabled by this option can be controversial in multiplayer ranked games as they can be perceived to providing better automation features then the one's in the base game GUI.
- Decrease performances of the mod if enabled.

### GUI Style

##### BoonGUI style:

Display a variety of stats, with an extensive panel, great for observers. Doesn't support EcoPanels

##### ProGUI style:

Display stats with emphasis on notifying you of idles units and buildings, this helps to manage your economy.

##### ModernGUI style:

Alternate between the best panels depending on wheras you are spectator or playing, and enable some extra features.

#### You can always change any options in settings. 
#### Note on colors : By default, the colors are set by the player's slot. You can change this in CustomColors Options

# Enjoy!


