initSummaryData = new Proxy(initSummaryData, {apply: function(target, thisArg, args) {
    target(...args);

    new CustomColors().updateGGameData();
}});
