/**
 * Transform a color object in byte format to a color object in decimal format
 *
 * @param {Object} color
 * @returns {Object}
 */
function rgbByteToDecimal(color)
{
    if (!("r" in color) || !("g" in color) || !("b" in color))
        return color;

    let ret = Object.assign({}, color);
    ret.r = color.r / 255;
    ret.g = color.g / 255;
    ret.b = color.b / 255;
    if ("a" in color)
        ret.a = color.a / 255;

    return ret;
}

/**
 * Transform a color object in decimal format to a color object in byte format
 *
 * @param {Object} color
 * @returns {Object}
 */
function rgbDecimalToByte(color)
{
    if (!("r" in color) || !("g" in color) || !("b" in color))
        return color;

    let ret = Object.assign({}, color);
    ret.r = color.r * 255;
    ret.g = color.g * 255;
    ret.b = color.b * 255;
    if ("a" in color)
        ret.a = color.a * 255;

    return ret;
}
