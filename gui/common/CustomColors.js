class CustomColors {

    constructor() {
        this.init();

        this.defaultColors = this.getDefaultColors();
        this.replacedColors = this.getReplacedColors();
        registerConfigChangeHandler(this.onConfigChange.bind(this));
    }

    init() {
        // Save default settings if they don't exist
        const settings = new CustomColorsSettings();
        settings.createDefaultSettingsIfNotExist();
    }

    getDefaultColors() {
        const settings = new CustomColorsSettings();
        const defaultSettings = settings.trim(settings.getDefault());
        // Here we are assuming that there's a 1-1 correspondence between settings keys and colors
        let ret = new Array(Object.keys(defaultSettings).length);
        Object.keys(defaultSettings).forEach(x => ret[+x - 1] = guiToRgbColor(defaultSettings[x]));
        return ret;
    }

    getReplacedColors() {
        const settings = new CustomColorsSettings();
        const savedSettings = settings.trim(settings.getSaved());
        // Here we are assuming that there's a 1-1 correspondence between settings keys and colors
        let ret = new Array(Object.keys(savedSettings).length);
        Object.keys(savedSettings).forEach(x => ret[+x - 1] = guiToRgbColor(savedSettings[x]));
        return ret;
    }
    substitute(index, color) {
        if (!color)
            color = this.defaultColors[index];
        return Engine.ConfigDB_GetValue("user", "customcolors.AllowMoreColors") != "false" ? this.substituteByColor(color) : this.substituteByIndex(index);
    }
    substituteByColor(color) {
        const index = this.defaultColors.findIndex(x => sameColor(x, color));
        if (index < 0)
            return color;
        return this.replacedColors[index];
    }

    substituteByIndex(index) {
        if (index == -1)
            return { r: 255, g: 255, b: 255, a: 100 };
        return this.replacedColors[index];
    }
    updateGPlayers() {
        if (Engine.ConfigDB_GetValue("user", "customcolors.replaceOwn") == "true" && !g_IsObserver) {
            this.viewedPlayerColor = Engine.ConfigDB_GetValue("user", "customcolors.ownReplacementColor") ? this.substituteByIndex(parseInt(Engine.ConfigDB_GetValue("user", "customcolors.ownReplacementColor")) - 1) : this.substituteByIndex(1);
            this.viewedPlayerOriginalColor = this.substitute(g_ViewedPlayer - 1, g_Players[g_ViewedPlayer].color);
            g_Players[g_ViewedPlayer].color = this.viewedPlayerColor;
        }
        g_Players.forEach((x, i) => {
            if (Engine.ConfigDB_GetValue("user", "customcolors.replaceOwn") == "true" && !g_IsObserver) {
                if (i !== g_ViewedPlayer) {
                    let substitutedColor = this.substitute(i - 1, x.color);
                    // Check if the substituted color is the g_ViewedPlayer color
                    if (substitutedColor === this.viewedPlayerColor) {
                        g_Players[i].color = this.viewedPlayerOriginalColor; // Use the initial color of the player
                    } else {
                        g_Players[i].color = substitutedColor;
                    }
                }
            }
            else
                g_Players[i].color = this.substitute(i - 1, x.color);
        });
    }
    updateGGameData() {
        g_GameData.sim.playerStates.forEach((x, i) => g_GameData.sim.playerStates[i].color = rgbByteToDecimal(this.substitute(i - 1, rgbDecimalToByte(x.color))));
    }

    updateGUI() {
        this.updateGPlayers();
        g_DiplomacyColors.updateDisplayedPlayerColors();
        g_PlayerViewControl.rebuild();
    }

    updatePlayerData() {
        this.defaultColors = this.getDefaultColors();
        g_CustomColors.updateGPlayers();
    }

    onConfigChange(changes) {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("customcolors."))) {
            const settings = new CustomColorsSettings();
            settings.createDefaultSettingsIfNotExist();
            this.defaultColors = [... this.replacedColors];
            this.replacedColors = this.getReplacedColors();
            if (global.g_Players){
                    this.updateGUI();
                    this.replacedColors = [];
                    g_Players.forEach((player, i) => {
                        this.replacedColors.push(player.color);
                    });
            }
                
        }
    }
}
