formatPlayerInfo = new Proxy(formatPlayerInfo, {apply: function(target, thisArg, args) {
    // Run original function if not in replay page
    if (!global.g_Replays)
        return target(...args);

    // Run additional code otherwise
    const customColors = new CustomColors();
    let playerDataArray = [...args[0]];
    playerDataArray.forEach((x, i) => x.Color = customColors.substitute(i, x.Color));
    return target(playerDataArray, ...args.slice(1));
}});
