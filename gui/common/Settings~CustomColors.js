/**
 * This class is responsible for dealing with settings.
 */
class CustomColorsSettings {

    getSaved() {
        const optionsJSON = Engine.ReadJSONFile("moddata/customcolors_option.json");
        let settings = {};
        optionsJSON.forEach(category => {
            category.options.forEach(option => {
                if (option.hasOwnProperty('val')) {
                    settings[option.config] = Engine.ConfigDB_GetValue("user", option.config);
                }
            });
        });

        return settings;
    }

    getDefault() {
        const optionsJSON = Engine.ReadJSONFile("moddata/customcolors_option.json");
        let settings = {};
        optionsJSON.forEach(category => {
            category.options.forEach(option => {
                if (option.hasOwnProperty('val')) {
                    settings[option.config] = option.val.toString();
                }
            });
        });
        return settings;
    }

    createDefaultSettingsIfNotExist() {
        const settings = this.getDefault();
        const modguiColors = Engine.ReadJSONFile("moddata/moderngui_default_config.json");
        
        switch (Engine.ConfigDB_GetValue("user", "customcolors.colorPreset")) {
            case "vanilla":
                Object.keys(settings).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
                break;
            case "moderngui":
                Object.keys(modguiColors).forEach(key => {
                    const isValidColorKey = /^customcolors\.\d+$/.test(key);
                    if (key.startsWith("customcolors.") && isValidColorKey) {
                        Engine.ConfigDB_CreateValue("user", key, modguiColors[key]);
                    }
                });
                break;
            default:
                Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
        }
    
        Engine.ConfigDB_CreateValue("user", "customcolors.colorPreset", "custom");
        Engine.ConfigDB_SaveChanges("user");
    }

    trim(settings) {
        const trimSize = "customcolors.".length;
        return Object.fromEntries(Object.entries(settings).map(x => [x[0].substring(trimSize), x[1]]));
    }

}
