var modernGUIConfig = {
	needsToSave: false,
	needsToReloadHotkeys: false,
	set: function (key, value) {
		Engine.ConfigDB_CreateValue("user", key, value);
		this.needsToSave = true;
		this.needsToReloadHotkeys = this.needsToReloadHotkeys || key.startsWith("hotkey.");
	},
	get: function (key) { return Engine.ConfigDB_GetValue("user", key) },
	save: function () {
		if (this.needsToSave) Engine.ConfigDB_SaveChanges("user", "config/user.cfg");
		if (this.needsToReloadHotkeys) Engine.ReloadHotkeys();
	}
};


function proGUI_initCheck() {
	const state = {
		"setUpProGUI": false
	};
	let settings = Engine.ReadJSONFile("moddata/moderngui_default_config.json");
	// Check settings
	{
		const allHotkeys = new Set(Object.keys(Engine.GetHotkeyMap()))
		// Normal check. Check for entries missing
		for (let key in settings) {
			if (key.startsWith("hotkey.")) {
				if (!allHotkeys.has(key.substring("hotkey.".length))) {
					modernGUIConfig.set(key, settings[key]);
				}
			}
			else if (modernGUIConfig.get(key) == "") {
				modernGUIConfig.set(key, settings[key]);
			}
		}
		modernGUIConfig.save();

	}
	// Check for the setUp page
	{
		const key = "moderngui.mainmenu.setupHelper.seen.1"
		if (modernGUIConfig.get(key) == "false") {
			state.setUpProGUI = true
		}
	}

	modernGUIConfig.save()
	return state;
}

let state = proGUI_initCheck();
if (Engine.ConfigDB_GetValue("user", "moderngui.mainmenu.setupHelper") == "true") {
	Engine.PushGuiPage("page_moderngui_welcome.xml");
}