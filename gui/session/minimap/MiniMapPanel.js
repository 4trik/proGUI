/**
 * This class is concerned with managing the different elements of the minimap panel.
 */
class MiniMapPanel {
	static Modes = [
		{ "name": "Beware", "icon": "alert", "message": "Pay attention to this position!" },
		{ "name": "Engage", "icon": "target", "message": "Engage the enemy here!" },
		{ "name": "Retreat", "icon": "run", "message": "Retreat from this position!" },
		{ "name": "Regroup", "icon": "focus", "message": "Regroup forces here." },
	];
	constructor(playerViewControl, diplomacyColors, idleWorkerClasses) {
		this.diplomacyColorsButton = new MiniMapDiplomacyColorsButton(diplomacyColors);
		this.diplomacyColorsButtonGUI = Engine.GetGUIObjectByName("diplomacyColorsButton");
		this.scoreButton = new MiniMapScoreButton();
		this.scoreButtonGUI = Engine.GetGUIObjectByName("scoreButton");
		this.flareButton = new MiniMapFlareButton(playerViewControl);
		this.flareButtonGUI = Engine.GetGUIObjectByName("flareButton");
		this.miniMapBarStats = new MiniMapBarStats();
		this.miniMap = new MiniMap();
		this.minimapPanel = Engine.GetGUIObjectByName("minimapPanel");
		this.minimapBG = Engine.GetGUIObjectByName("minimapBG");
		this.minimapCircle = Engine.GetGUIObjectByName("minimapCircle");
		this.minimapMap = Engine.GetGUIObjectByName("minimap");
		this.hoverPanel = Engine.GetGUIObjectByName("hoverPanel");
		this.powerBar = Engine.GetGUIObjectByName("powerBar");
		this.powerPanel = Engine.GetGUIObjectByName("powerPanel");
		this.powerGlow = Engine.GetGUIObjectByName("miniMapBarStatsGlow");
		this.flareMenu = Engine.GetGUIObjectByName("flarepanel");
		// StatsOverlay has no size defined, it serves to get the screen size, maybe there is a better way.
		this.stats = Engine.GetGUIObjectByName("Stats");
		this.inputMinRefresh = 200;
		this.getSettings();
		this.rebuild();
		this.resizeMiniMap();
		this.minimapPanel.onWindowResized = this.resizeMiniMap.bind(this);
		playerViewControl.registerViewedPlayerChangeHandler(this.rebuild.bind(this));
		registerConfigChangeHandler(this.onConfigChange.bind(this));

    }
    onConfigChange(changes) {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("moderngui.minimap")))
            this.getSettings();
		this.rebuild();
		g_stats.toggle(false); //Bug when init 'score' button;
    }
    getSettings() {
		this.miniMapPerk = Engine.ConfigDB_GetValue("user", "moderngui.minimap.perks");
		this.miniMapSize = Engine.ConfigDB_GetValue("user", "moderngui.minimap.size");
		this.miniMapCiVBG = Engine.ConfigDB_GetValue("user", "moderngui.minimap.civbg") == "true";
		this.toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "moderngui.trainer.color") + ".dds";
	}
	rebuild() {

		if (this.miniMapPerk == "FlareMenu" && g_IsNetworked && !g_IsObserver) {
			this.flareMenu.hidden = false;
			this.currentlyCheckedIndex = -1;
			this.flareMessage = MiniMapPanel.Modes[0].message;

			for (let i = 0; i < MiniMapPanel.Modes.length; i++) {
				const mode = MiniMapPanel.Modes[i] ?? null;
				const icon = Engine.GetGUIObjectByName(`flare[${i}]_Icon`);
				const button = Engine.GetGUIObjectByName(`flare[${i}]`);
				const name = Engine.GetGUIObjectByName(`flare[${i}]_Name`);
				name.caption = MiniMapPanel.Modes[i].message;
				if (icon) {
					button.size = `100%+8 -172+${40 * (i)} 100%+8+40 -172+${40 * (i + 1)}`;
					icon.sprite = `stretched:session/phosphor/${mode.icon}.png`;
				}
				button.onPress = () => {
					this.onPress(i);
				};
				button.tooltip = mode.message;
			}
		}
		if (g_IsObserver && this.flareMenu)
			this.flareMenu.hidden = true;
		this.setCivBackgroundTexture();
		this.resizeMiniMap();
	}
	setCivBackgroundTexture() {
		if (this.miniMapCiVBG) {
			const playerCiv = g_ViewedPlayer > 0 ? g_Players[g_ViewedPlayer].civ : "gaia";
			this.minimapBG.sprite = `stretched:session/icons/bkg/background_circle_${playerCiv}.png`;
		}
	}
	async onPress(index) {
		if (inputState == INPUT_NORMAL || inputState == INPUT_FLARE) {
			//Single Selection option from a list of options.
			if (this.currentlyCheckedIndex !== -1) {
				const previouslyCheckedIcon = Engine.GetGUIObjectByName(`flare[${this.currentlyCheckedIndex}]`);
				previouslyCheckedIcon.checked = false;
				Engine.GetGUIObjectByName(`flare[${this.currentlyCheckedIndex}]_Background`).sprite = "snIconPortrait";
			}
			this.currentlyCheckedIndex = index;
			const currentlyCheckedMode = Engine.GetGUIObjectByName(`flare[${index}]`);
			currentlyCheckedMode.checked = true;
			Engine.GetGUIObjectByName(`flare[${index}]_Background`).sprite = this.toggledIcon;
			this.flareMessage = MiniMapPanel.Modes[index].message;
			let channel = "/allies ";
			let message = this.flareMessage;
			if (inputState == INPUT_NORMAL) {
				inputState = INPUT_FLARE;
				await new Promise(resolve => setTimeout(resolve, this.inputMinRefresh));
				this.checkInputStateAndExecute(channel, message);
			}
		}
		else {
			warn("You are currently busy, can't send a flare.");
			Engine.GetGUIObjectByName(`flare[${index}]_Background`).sprite = "snIconPortrait";
			Engine.GetGUIObjectByName(`flare[${index}]`).checked = false;
		}
	}
	// Send the message when the input state is normal
	async executeWhenInputNormal(channel, message) {
		Engine.SendNetworkChat(channel + this.flareMessage);
		for (let i = 0; i < 4; i++) {
			Engine.GetGUIObjectByName(`flare[${i}]`).enabled = false;
			Engine.GetGUIObjectByName(`flare[${i}]`).checked = true;
			if (i != this.currentlyCheckedIndex)
				Engine.GetGUIObjectByName(`flare[${i}]_Background`).sprite = "ModernDarkBoxDisabled";
		}
		await new Promise(resolve => setTimeout(resolve, g_FlareCooldown - this.inputMinRefresh - 300)); //removed 300 because user might not place flare in this interval anyway

		for (let i = 0; i < 4; i++) {
			Engine.GetGUIObjectByName(`flare[${i}]`).enabled = true;
			Engine.GetGUIObjectByName(`flare[${i}]_Background`).sprite = "snIconPortrait";
			Engine.GetGUIObjectByName(`flare[${i}]`).checked = false;
		}
	}

	// Check the input state and send the message the code if it's normal
	async checkInputStateAndExecute(channel, message) {
		if (inputState != INPUT_FLARE) {
			this.executeWhenInputNormal(channel, message);
		} else {
			setTimeout(() => this.checkInputStateAndExecute(channel, message), 100); // Check again after 100 milliseconds
		}
	}


	flare(target, playerID) {
		return this.miniMap.flare(target, playerID);
	}

	isMouseOverMiniMap() {
		return this.miniMap.isMouseOverMiniMap();
	}

	resizeMiniMap() {
		if (this.miniMapPerk != "StatBars") {
			this.powerBar.hidden = true;
			this.powerPanel.hidden = true;
			this.stats.hidden = true;
			this.powerGlow.hidden = true;
			this.powerGlow.ghost = true;
			this.hoverPanel.sprite = "minimapHoverPanel";
			this.minimapBG.size = "3 3 100% 100%";
			this.minimapMap.size = "2 2 100% 100%";
			this.minimapCircle.size = "0 0 100%+1 100%";
			this.diplomacyColorsButtonGUI.size = "72% 40%-1 86% 94%";
			this.scoreButtonGUI.size = "2%-1 6% 16%-1 60%+1";
			this.flareButtonGUI.size = "14% 40%-1 28% 94%";
			const dimensionsMiniPanel = "0 100%-276 250 100%-26";
			const dimensionsHoverPanel = "0 100%-63 251 100%";
			const screenSizeWidth = this.stats.getComputedSize().right;
			var minmapSize = this.miniMapSize;
			// arbitrarily set to 1600, just felt right
			if (screenSizeWidth >= 1600)
				minmapSize = minmapSize + 1;
			this.minimapPanel.size = `0 100%-${276 + 86 * minmapSize} 250+${80 * minmapSize} 100%-${26 + 9 * minmapSize}` //"0 100%-362 330 100%-35"; //
			this.hoverPanel.size = `0 100%-${63 + 18 * minmapSize} ${251 + 80 * minmapSize} 100%` //"0 100%-76 301 100%"; //

		}
		else if (this.miniMapPerk == "StatBars") {
			this.flareMenu.hidden = true;
			this.powerBar.hidden = false;
			this.powerPanel.hidden = false;
			this.stats.hidden = false;
			this.powerGlow.hidden = false;
			this.powerGlow.ghost = false;
			this.hoverPanel.sprite = "minimapHoverPanelStats";
			this.minimapBG.size = "70 -15 100%+71 100%-15";
			this.minimapMap.size = "70 -15 100%+71 100%-15";
			this.minimapCircle.size = "70 -15 100%+71 100%-15";
			this.diplomacyColorsButtonGUI.size = "73%+80 54%-22 87%+79 108%-22";
			this.scoreButtonGUI.size = "18%+44 54%-22 32%+43 108%-22";
			this.flareButtonGUI.size = "86%+80 14%-22 100%+80 68%-22";
			const dimensionsMiniPanel = "-4 100%-276 250 100%-20";
			const dimensionsHoverPanel = "-4 100%-60 251 100%+4";
			const screenSizeWidth = this.stats.getComputedSize().right;
			this.minimapPanel.size = dimensionsMiniPanel;
			this.hoverPanel.size = dimensionsHoverPanel;
			this.powerBar.size = "286 100%-860 15 100%";
		}
	}
}
