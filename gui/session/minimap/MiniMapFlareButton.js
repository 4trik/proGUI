/**
 * If the button that this class manages is pressed, the input state is switched to 'flare', letting the player send a flare by left-clicking on the map.
 * 
 * 
 * In proGUI we fix a bug where the sprite is set by this class
 */
class MiniMapFlareButton
{
	constructor(playerViewControl)
	{
		this.flareButton = Engine.GetGUIObjectByName("flareButton");
		this.flareButton.onPress = this.onPress.bind(this);
		registerHotkeyChangeHandler(this.onHotkeyChange.bind(this));
		playerViewControl.registerViewedPlayerChangeHandler(this.rebuild.bind(this));
	}

	rebuild()
	{
	}

	onHotkeyChange()
	{
		this.colorizedHotkey = colorizeHotkey("%(hotkey)s" + " ", "session.flare");
		this.updateTooltip();
	}

	updateTooltip()
	{
		this.flareButton.tooltip = this.colorizedHotkey + (g_IsObserver ? this.ObserverTooltip : this.PlayerTooltip);
	}

	onPress()
	{
		if (inputState == INPUT_NORMAL)
			inputState = INPUT_FLARE;
	}
}

MiniMapFlareButton.prototype.PlayerTooltip = markForTranslation("Send a flare to your allies.");
MiniMapFlareButton.prototype.ObserverTooltip = markForTranslation("Send a flare to other observers.");
