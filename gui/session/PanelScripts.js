class PanelScripts {

    constructor() {
        this.modes = new Array;
        this.allyToTribute = new Array;
        this.playerEntities = new Object;
        this.playersStates = this.getPlayersStates();
        this.hasBeenUpdated = { "stats": true, "pop": true };
        this.lastUpdateTime = 0;
        this.turnLength = 200;
        this.hasMarket = false;
        this.moderngui_fullupdate_interval = 1800;
        this.moderngui_statsupdate_interval = 1200;
        this.moderngui_fullupdate_last = 0;
        this.moderngui_statsupdate_last = 0;
        this.turnLength = 400;
        this.selfCorrectTimer = 250; //Actualy not going to use turn length but a hack to make a refresh even when timeElapsed is slowed or paused.
        this.lastUpdateTimeScript = -1000; //Negative to initiate before turn 1;
        this.lastPriceUpdate = {
            "food": 0,
            "wood": 0,
            "stone": 0,
            "metal": 0
        };
        this.resource = {
            "counts": {},
            "gatherers": {},
            "rates": {}
        };
        this.userRefRes = {
            "food": 0,
            "wood": 0,
            "stone": 0,
            "metal": 0
        };
        this.modes = this.getModes();
        this.unitsToIgnore = 0;
        this.getSettings();
        registerConfigChangeHandler(this.onConfigChange.bind(this));

    }
    onConfigChange(changes) {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("moderngui.trainer")))
            this.getSettings();
    }
    getSettings() {
        this.toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "moderngui.trainer.color") + ".dds";
        this.tributeReserve = Math.floor(Engine.ConfigDB_GetValue("user", "moderngui.trainer.tributeReserve"));
        this.shouldOverTrain = Engine.ConfigDB_GetValue("user", "moderngui.trainer.strictCompo") == "true";
        this.pushInFrontIfTimeRemaining = Math.round(Engine.ConfigDB_GetValue("user", "moderngui.trainer.pushOrderIfFrontTimeRemaining"));
        this.woodUpgradeP1 = Math.round(parseFloat(Engine.ConfigDB_GetValue("user", "moderngui.trainer.techPanel.woodStorehouseP1")));
        this.farmUpgradeP1 = Math.round(parseFloat(Engine.ConfigDB_GetValue("user", "moderngui.trainer.techPanel.woodFarmP1")));
        this.trainerReverPriority = Engine.ConfigDB_GetValue("user", "moderngui.trainer.reversePriority") == "true";
    }
    getUnspendableResources() { // Used?
        return {

            "food": Math.floor(Engine.ConfigDB_GetValue("user", "moderngui.trainer.foodReserve")),
            "wood": Math.floor(Engine.ConfigDB_GetValue("user", "moderngui.trainer.woodReserve")),
            "stone": Math.floor(Engine.ConfigDB_GetValue("user", "moderngui.trainer.stoneReserve")),
            "metal": Math.floor(Engine.ConfigDB_GetValue("user", "moderngui.trainer.metalReserve"))
        }
    }
    getModes() {
        const modes = [];
        const numModes = Engine.GetGUIObjectByName("StatsModesTabButtons").children.length;
        this.unitsToIgnore = 0;
        const prefix = "stretched:session/portraits/";
        for (let i = 0; i < numModes; i++) {
            const unitFromTab = Engine.GetGUIObjectByName(`StatsModesTabButton[${i}]_Name`);
            let name = unitFromTab.caption;

            const value = Number.parseFloat(Engine.GetGUIObjectByName(`StatsModesTabButton[${i}]_Count`).caption);
            const checked = (Engine.GetGUIObjectByName(`StatsModesTabButton[${i}]_Background`).sprite == this.toggledIcon) ? true : false;
            modes.push({
                name,
                value,
                checked,
                count: 0
            });
        }
        return modes;
    }
    getPlayersStates() {
        const playersStates = Engine.GuiInterfaceCall("moderngui_GetOverlay", {
            g_IsObserver, g_ViewedPlayer, g_LastTickTime
        }).players ?? [];
        return playersStates;
    }
    updateMarketPrices() {
        let i = 0;
        let prices = GetSimState().players[g_ViewedPlayer].barterPrices;
        for (const resType of g_BoonGUIResTypes) {
            this.sellPrice = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][${4 + i}]_Count`);
            i++;
            if (Number.parseFloat(this.sellPrice.caption) == Math.round(prices.sell[resType]) && this.gameTime > this.lastPriceUpdate[resType] + 600)
                this.sellPrice.textcolor = "255 255 255";
            else if (Number.parseFloat(this.sellPrice.caption) != Math.round(prices.sell[resType])) {
                this.sellPrice.textcolor = (this.sellPrice.caption < prices.sell[resType]) ? "55 255 55" : "255 55 55";
                this.lastPriceUpdate[resType] = this.gameTime;
            }
            this.sellPrice.caption = Math.round(prices.sell[resType]);
            if (this.hasMarket)
                this.sellPrice.hidden = false;
            else
                this.sellPrice.hidden = true;

        }
    }
    updatePlayersStates() {
        try {
            this.playersStates = [...g_stats.playersStates];
            this.userState = { ...g_stats.userState };
        } catch (e) {
            //warn("Loading from backup");
            this.playersStates = this.getPlayersStates();
            this.userState = this.playersStates.find(x => x.index === g_ViewedPlayer);
        }
        this.idleTrainers = this.getNumberOfIdleTrainers();
    }

    // This function is responsible for executing a series of tasks during gameplay.
    tickTasks() {
        this.gameTime = g_SimState.timeElapsed;
        // Execute these tasks only if the player is not an observer.
        if (!g_IsObserver) {
            // Check if preparing for next phase.
            this.preparingNextPhase = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][8]_Background`).sprite == this.toggledIcon;
            // Get the states of all players and filter by the currently viewed player.
            if (this.lastUpdateTime + this.turnLength <= this.gameTime) {
                this.checkIfUpdated();
                this.modes = this.getModes(); //Variables reset here.
                this.updatePlayersStates();
                this.updateMarketPrices();
                this.playerEntities = this.getPlayerEntities(); //Get Variables.
                this.lastUpdateTime = this.gameTime;
            }
            if (this.lastUpdateTimeScript + this.selfCorrectTimer <= this.gameTime) {
                // If not preparing for next phase, perform these tasks.
                if (this.preparingNextPhase == false) {
                    // If the control panel tab button icon matches the toggledIcon, train citizens and check for updates.
                    if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][0]_Background`).sprite == this.toggledIcon && !this.userState?.trainingBlocked) {
                        this.trainCitizens();
                    }
                    // Research technologies and check for updates.
                    this.researchTechnologies();
                    // If the control panel tab button icon matches the toggledIcon, help allies tribute and check for updates.
                    if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][3]_Background`).sprite == this.toggledIcon) {
                        this.helpAlliesTribute();
                    }
                }
                // If preparing for next phase, perform these tasks.
                else if (this.preparingNextPhase == true) {
                    // Get player entities and researcher technologies.
                    this.prepareNextPhase();
                }
                this.lastUpdateTimeScript = this.gameTime;
            }
            this.selfCorrectTimer -= Engine.GetFPS() * 5;
        }
    }
    prepareNextPhase() {
        if (this.userState.civCentres[0]) { //TODO Should consider all CC
            let playerEntity = this.userState.civCentres[0];
            let state = GetEntityState(playerEntity);
            if (!state)
                return;
            if (hasClass(state, "Fundation"))
                return;
            let reseachableTechs = state.researcher.technologies;
            let techName = "phase_";
            let tech = this.getTechFromTechs(techName, reseachableTechs);
            // If a researchable technology is available, make the research and toggle the control panel mode.
            if (tech) {
                if (this.makeResearch(playerEntity, tech, true)) {
                    g_stats.controlPanel.toggleMode(8, true);
                    this.moderngui_fullupdate_last = this.moderngui_statsupdate_last = this.gameTime;
                }
            }
        }
    }
    // This function checks if an entity has been updated recently by comparing its state to the previous time it was ordered.
    checkIfUpdated() {
        if (this.gameTime - this.moderngui_fullupdate_last >= this.moderngui_fullupdate_interval) {
            this.hasBeenUpdated.pop = true;
            this.moderngui_fullupdate_last = this.gameTime;
        }
        if (this.gameTime - this.moderngui_statsupdate_last >= this.moderngui_statsupdate_interval) {
            this.hasBeenUpdated.stats = true;
            this.moderngui_statsupdate_last = this.gameTime;
        }
    }

    // This function returns the number of idle trainers by checking their production queue status
    getNumberOfIdleTrainers() {
        let num = 0;
        // Loop through all the Trainer entities owned by the player
        for (let trainerID in this.playerEntities?.Trainer) {
            // Get the state of each Trainer entity
            let state = GetEntityState(this.playerEntities.Trainer[trainerID]);

            // Check if the production queue of the Trainer entity is empty or if there is only one item in the queue and its progress is greater than 0.9
            if (state?.production?.queue.length === 0 || (state?.production?.queue.length === 1 && (state?.production?.queue[0].progress > 0.8 || state?.production?.queue[0].timeRemaining < 4200))) {
                let trainableUnitsList = this.getTrainableUnitsFromTrainer(state);

                for (let trainableUnit of trainableUnitsList) {
                    let matchingMode = this.modes.find(x => x.name == trainableUnit);
                    if (hasClass(state, "Corral") && Engine.GetGUIObjectByName(`ControlPanelTabButton[1][5]_Background`).sprite == this.toggledIcon && this.userState.classCounts.Animal < 50) {
                        num++;
                        break;
                    }
                    if (matchingMode != undefined) {
                        // If the Trainer is idle and has a unit that's selected for training, increment the count
                        if (matchingMode.checked == true || matchingMode.value != 0) {
                            num++;
                            break;
                        }
                    }
                }
            }
        }
        // Return the number of idle Trainers
        return num;
    }

    setNotToTribute(playerNick) {
        this.allyToTribute = this.allyToTribute.filter(x => x != playerNick);
    }
    setToTribute(playerNick) {
        this.allyToTribute.push(playerNick);
    }
    // This function is for tributing resources to allied players.
    helpAlliesTribute() {

        // Check if the user states have been updated and if player states exist
        if (!this.userState || !this.hasBeenUpdated.stats)
            return
        // Loop through each player state
        checkAllies:
        for (let playerStates of this.playersStates) {
            if (playerStates.offline)
                break;
            let state = playerStates;
            // Check if the player is in the list of allies to tribute
            if (this.allyToTribute.indexOf(splitRatingFromNick(playerStates.name).nick) != -1) {
                // Loop through each resource type to be shared
                for (const resType of g_BoonGUIResTypes) {
                    // Get the current amount of the resource for the player being checked and the user
                    let value = state.resourceCounts[resType];
                    let userRes = this.userState.resourceCounts[resType];
                    let userGatherers = this.userState.resourceGatherers[resType];
                    let shareratio = 0;

                    // Get the share ratio for the resource based on the toggled icon and resource type
                    if (resType == "food")
                        shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][0]_Background`).sprite == this.toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][0]_Count`).caption : 0;
                    else if (resType == "wood")
                        shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][1]_Background`).sprite == this.toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][1]_Count`).caption : 0;
                    else if (resType == "stone")
                        shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][2]_Background`).sprite == this.toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][2]_Count`).caption : 0;
                    else if (resType == "metal")
                        shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][3]_Background`).sprite == this.toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][3]_Count`).caption : 0;
                    // Check if the user resource count has changed
                    if (this.userRefRes[resType] != userRes) {

                        // Check if both the player being checked and the user have resources and gatherers for the resource type
                        if (userRes != undefined && userGatherers != undefined) {
                            // calcResOptiulate the gatherer ratio of the player being checked to the user
                            let gatherratio = (state.resourceGatherers[resType] + 1) / (userGatherers + 1);
                            // Get the tribute reserve specified in the config
                            const resReserve = this.tributeReserve;

                            // calcResOptiulate the optimal amount to tribute based on share ratio, player resources and gatherers, and user resources
                            let calcResOpti = Math.min(Math.round((((userRes - resReserve) - value - resReserve * gatherratio) * shareratio)), 500)
                            if (userRes > resReserve + calcResOpti && calcResOpti > 100 && value < 1000 && value < userRes * shareratio) {
                                // If the optimal amount to tribute is greater than 100, send a tribute command
                                let playerID = state.index;
                                this.userRefRes[resType] = userRes;

                                Engine.PostNetworkCommand({
                                    "type": "tribute",
                                    "player": playerID,
                                    "amounts": {
                                        [resType]: calcResOpti
                                    }
                                });
                                this.userState.resourceCounts[resType] -= calcResOpti;
                                this.hasBeenUpdated.stats = false;
                                this.moderngui_statsupdate_last = this.gameTime;
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    getRemainingUnitsToTrain(template) {
        const matchingMode = this.modes.find(x => x.name === template);
        if (matchingMode) {
            if (matchingMode.checked)
                return this.userState.popLimit * 1;
            let currentRatio = matchingMode.count / (this.userState.popCount - this.unitsToIgnore);
            if (currentRatio > matchingMode.value / 100) {
                if (matchingMode.value >= 1) {
                    let shouldOverTrain = (this.shouldOverTrain) ? 0 : (1 - (currentRatio - matchingMode.value / 100));
                    return shouldOverTrain;
                }

                else
                    return 0;
            }
            return this.userState.popLimit * (matchingMode.value / 100) - matchingMode.count;
        } else {
            return 0;
        }
    }
    getTrainableUnitsCorrals(template) {
        let unitCost = GetTemplateData(template).cost;
        let trainableUnits = parseInt(Engine.GetGUIObjectByName(`ControlPanelTabButton[1][5]_Count`).caption);
        let i = 0;
        for (const resType of g_BoonGUIResTypes) {
            let unspendatbleResources = 0;
            let spendableRes = this.userState.resourceCounts[resType] - unspendatbleResources;
            if (spendableRes < 0)
                spendableRes = 0;
            if (trainableUnits > spendableRes / unitCost[resType]) {
                trainableUnits = Math.floor(spendableRes / unitCost[resType]);
            }
            i++;
        }
        if (this.userState.classCounts.Animal * 1 + trainableUnits * 1 > 50) { //HARDCODED animal limit !
            trainableUnits = 50 - this.userState.classCounts.Animal;
        }
        if (trainableUnits > 1 && this.idleTrainers > 1)
            trainableUnits = Math.max(Math.floor(trainableUnits / this.idleTrainers), 1);
        return trainableUnits;
    }
    getTrainableUnits(template) {
        let templateData = GetTemplateData(template);
        let unitCost = templateData.cost;
        let trainableUnits = Math.floor(this.userState.popLimit - this.userState.popCount / unitCost.population);
        if (template.includes("war_dog")) { //Dogs in a27 Todo something general about limits.
            trainableUnits = this.userState.classCounts.Kennel * 10 - this.userState.classCounts.Dog;
        }
        let i = 0;
        for (const resType of g_BoonGUIResTypes) {
            let unspendableResources = (Engine.GetGUIObjectByName(`ControlPanelTabButton[1][${i}]_Background`).sprite == this.toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[1][${i}]_Count`).caption : 0;
            let spendableRes = this.userState.resourceCounts[resType] - unspendableResources;
            if (inputState != 0 && placementSupport?.template)
                spendableRes = this.userState.resourceCounts[resType] - Math.max(unspendableResources, GetTemplateData(placementSupport.template).cost[resType]);
            if (spendableRes < 0)
                spendableRes = 0;
            if (trainableUnits > spendableRes / unitCost[resType]) {
                trainableUnits = Math.floor(spendableRes / unitCost[resType]);
            }
            i++;
        }
        return trainableUnits;
    }
    splitTrainableUnitsAndCheck(template, trainableUnits) {
        if (template) {
            let unitCost = GetTemplateData(template).cost;
            const maxTrainingBatchSize = Engine.GetGUIObjectByName(`ControlPanelTabButton[1][4]_Count`).caption;
            if (trainableUnits > 1 && this.idleTrainers > 1)
                trainableUnits = Math.max(Math.floor(trainableUnits / this.idleTrainers), 1);
            if (trainableUnits > Math.ceil(maxTrainingBatchSize / (unitCost.population)))
                trainableUnits = Math.ceil(maxTrainingBatchSize / (unitCost.population));
            if (trainableUnits * unitCost.population > this.userState.popLimit - this.userState.popCount && trainableUnits >= 1)
                trainableUnits = Math.floor((this.userState.popLimit - this.userState.popCount) / unitCost.population);
            return trainableUnits;
        }
        return 0;
    }
    canResearch(unitCost) {

        for (const resType of g_BoonGUIResTypes) {
            let spendableRes = this.userState.resourceCounts[resType];
            if (inputState != 0 && placementSupport?.template)
                spendableRes -= GetTemplateData(placementSupport.template).cost[resType];
            if (spendableRes < 0)
                spendableRes = 0;
            if (unitCost[resType] > spendableRes) {
                return false;
            }
        }
        return true;
    }
    makeResearch(entity, tech, priority) {
        let isTechnologyResearchable = Engine.GuiInterfaceCall("CheckTechnologyRequirements", {
            "tech": tech,
            "player": this.userState.index
        });
        let state = GetEntityState(entity);
        if (priority == true && state?.production?.queue[0]?.timeRemaining != undefined)
            if (state.production.queue[0].timeRemaining <= this.pushInFrontIfTimeRemaining)
                priority = false;
        let hasResForResearch = this.canResearch(TechnologyTemplates.Get(tech).cost);
        if (hasResForResearch && isTechnologyResearchable) {
            Engine.PostNetworkCommand({
                "type": "research",
                "entity": entity,
                "template": tech,
                "pushFront": priority
            });
            this.hasBeenUpdated.stats = false;
            this.moderngui_fullupdate_last = this.moderngui_statsupdate_last = this.gameTime;
            return true;

        }
        return false;
    }
    researchTechnologies() {

        research:
        if (this.hasBeenUpdated.stats) {
            for (let researcherID in this.playerEntities.Researcher) {
                let playerEntity = this.playerEntities.Researcher[researcherID]
                let state = GetEntityState(playerEntity);
                if (!state)
                    return;
                if (state.researcher
                    && this.userState && (state.production.queue.length === 0 || (state.production.queue.length === 1 && state.production.queue[0].timeRemaining <= 1000))) {
                    let reseachableTechs = state.researcher.technologies;
                    //forge:
                    if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][1]_Background`).sprite == this.toggledIcon) {

                        if (hasClass(state, "Forge")) {
                            for (let i = 3; i != -1; i--) { // i the number of techs we look for.
                                if (Engine.GetGUIObjectByName(`ControlPanelTabButton[2][${i}]_Background`).sprite != this.toggledIcon)
                                    continue;
                                let techName = Engine.GetGUIObjectByName(`ControlPanelTabButton[2][${i}]_Order`).caption;
                                let tech = this.getTechFromTechs(techName, reseachableTechs);
                                if (tech)
                                    if (tech.split('_0')[1]?.slice(0, 1) <= Engine.GetGUIObjectByName(`ControlPanelTabButton[2][${i}]_Count`).caption.length || !tech.split('_0')[1])
                                        if (this.makeResearch(playerEntity, tech, false))
                                            break research;
                            }
                        }
                    }
                    if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][2]_Background`).sprite == this.toggledIcon) {
                        //Storehouse:
                        let triggerWoodAmount = this.woodUpgradeP1;
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][0]_Background`).sprite == this.toggledIcon) {
                            if ((hasClass(state, "Storehouse") && (this.userState.phase != "village" || this.userState.resourceCounts.wood >= triggerWoodAmount))) {
                                let techName = Engine.GetGUIObjectByName(`ControlPanelTabButton[3][0]_Order`).caption;
                                let tech = this.getTechFromTechs(techName, reseachableTechs);
                                if (tech) {
                                    let techIconName = TechnologyTemplates.Get(tech)?.icon;
                                    if (techIconName.split('_0')[1]?.slice(0, 1) <= Engine.GetGUIObjectByName(`ControlPanelTabButton[3][0]_Count`).caption.length || !techIconName.split('_0')[1])
                                        if (this.makeResearch(playerEntity, tech, false))
                                            break research;
                                }
                            }
                        }
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][2]_Background`).sprite == this.toggledIcon) {
                            if ((hasClass(state, "Storehouse"))) {
                                let techName = Engine.GetGUIObjectByName(`ControlPanelTabButton[3][2]_Order`).caption;
                                const matchingTechs = this.getTechsFromTechs(techName, reseachableTechs);
                                for (const tech of matchingTechs) {
                                    if (tech) {
                                        let techIconName = TechnologyTemplates.Get(tech)?.icon;

                                        if (techIconName?.includes("stone")) {
                                            if (techIconName.split('_0')[1]?.slice(0, 1) <= Engine.GetGUIObjectByName(`ControlPanelTabButton[3][2]_Count`).caption.length || !techIconName.split('_0')[1]) {
                                                if (this.makeResearch(playerEntity, tech, false)) {
                                                    break research;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][3]_Background`).sprite == this.toggledIcon)
                            if ((hasClass(state, "Storehouse"))) {
                                let techName = Engine.GetGUIObjectByName(`ControlPanelTabButton[3][3]_Order`).caption;
                                const matchingTechs = this.getTechsFromTechs(techName, reseachableTechs);
                                for (const tech of matchingTechs) {
                                    if (tech) {
                                        let techIconName = TechnologyTemplates.Get(tech)?.icon;
                                        if (techIconName?.includes("metal")) {
                                            if (techIconName.split('_0')[1]?.slice(0, 1) <= Engine.GetGUIObjectByName(`ControlPanelTabButton[3][3]_Count`).caption.length || !techIconName.split('_0')[1]) {
                                                if (this.makeResearch(playerEntity, tech, false)) {
                                                    break research;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        triggerWoodAmount = this.farmUpgradeP1;
                        //farmstead:
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][4]_Background`).sprite == this.toggledIcon) {
                            if ((hasClass(state, "Farmstead"))) {
                                let techName = Engine.GetGUIObjectByName(`ControlPanelTabButton[3][4]_Order`).caption;
                                let tech = this.getTechFromTechs(techName, reseachableTechs);;
                                if (tech) {
                                    if (this.makeResearch(playerEntity, tech, false))
                                        break research;
                                }
                            }
                        }
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][1]_Background`).sprite == this.toggledIcon) {
                            if ((hasClass(state, "Farmstead") && (this.userState.phase != "village" || this.userState.resourceCounts.wood >= triggerWoodAmount))) {
                                let techName = Engine.GetGUIObjectByName(`ControlPanelTabButton[3][1]_Order`).caption;
                                let tech = this.getTechFromTechs(techName, reseachableTechs);
                                if (tech)
                                    if (this.makeResearch(playerEntity, tech, false))
                                        break research;
                            }
                        }
                    }
                }
            }
        }
    }
    getTechFromTechs(techName, reseachableTechs) {
        const found = reseachableTechs.find(str => {
            if (typeof str === 'string' && str.includes(techName)) {
                return true; // Indicate a match for the string
            }
            if (typeof str === 'object' && str !== null) {
                // Check the "top" property
                if (str.top && str.top.includes(techName)) {
                    return str.top; // This will return the matched string from "top"
                }
                // Check the "bottom" property
                if (str.bottom && str.bottom.includes(techName)) {
                    return str.bottom; // This will return the matched string from "bottom"
                }
            }
            return false; // No match found
        });

        // If found is a string, return it; otherwise, return undefined
        if (typeof found === 'string') {
            return found; // Return the matched string
        } else if (typeof found === 'object' && found !== null) {
            // If found is an object, check if it has "top" or "bottom" that matched
            if (found.top && found.top.includes(techName)) {
                return found.top; // Return the matched string from "top"
            }
            if (found.bottom && found.bottom.includes(techName)) {
                return found.bottom; // Return the matched string from "bottom"
            }
        }
        return undefined; // Return undefined if no match is found
    }
    getTechsFromTechs(techName, reseachableTechs) {
        const matches = []; // Array to hold all matches

        reseachableTechs.forEach(str => {
            if (typeof str === 'string' && str.includes(techName)) {
                matches.push(str); // Add the matched string to the array
            } else if (typeof str === 'object' && str !== null) {
                // Check the "top" property
                if (str.top && str.top.includes(techName)) {
                    matches.push(str.top); // Add the matched string from "top" to the array
                }
                // Check the "bottom" property
                if (str.bottom && str.bottom.includes(techName)) {
                    matches.push(str.bottom); // Add the matched string from "bottom" to the array
                }
            }
        });

        // Return the array of matches, or an empty array if no matches were found
        return matches;
    }


    getTrainableUnitsFromTrainer(state) {
        if (state?.trainer) {
            let trainableUnitsList = this.trainerReverPriority ? [...state.trainer.entities] : [...state.trainer.entities].reverse();
            if (Array.isArray(trainableUnitsList))
                return trainableUnitsList;
            else
                return [];
        }
    }
    trainCitizens() {
        // Check if updates are needed
        if (!this.hasBeenUpdated.pop || !this.hasBeenUpdated.stats) return;

        // Iterate through each trainer
        for (let trainerID in this.playerEntities.Trainer) {
            const state = GetEntityState(this.playerEntities.Trainer[trainerID]);
            let unitToTrain = false;
            let unitToTrainQty = 0;
            let unitToTrainQtyMax = 0;

            // Check if the trainer is ready to train units
            if ((state?.production?.queue[0]?.unitTemplate !== undefined || state?.production?.queue.length === 0) &&
                !state.production.autoqueue && !state?.upgrade?.isUpgrading) {

                if (state?.trainer && this.userState &&
                    Engine.GetGUIObjectByName(`ControlPanelTabButton[1][4]_Background`).sprite === this.toggledIcon) {

                    const trainableUnitsList = this.getTrainableUnitsFromTrainer(state);

                    for (let trainableUnit of trainableUnitsList) {
                        const matchingMode = this.modes.find(x => x.name === trainableUnit);
                        if (matchingMode) {
                            const requirements = GetTemplateData(trainableUnit).requirements;

                            // Check if technology requirements are met
                            if (requirements) {
                                const technologyEnabled = Engine.GuiInterfaceCall("AreRequirementsMet", {
                                    requirements,
                                    player: this.userState.index
                                });
                                if (!technologyEnabled) continue;
                            }

                            const thisToTrainQty = this.getTrainableUnits(trainableUnit);
                            const thisToTrainQtyMax = this.getRemainingUnitsToTrain(trainableUnit);

                            // Determine if this unit is a better candidate for training
                            if ((((thisToTrainQtyMax > unitToTrainQtyMax) && (thisToTrainQtyMax > 0 && thisToTrainQty > 0)) ||
                                (matchingMode.checked && thisToTrainQty >= 1))) {

                                unitToTrain = trainableUnit;
                                unitToTrainQty = thisToTrainQty; // Do not limit batch for composition
                                unitToTrainQtyMax = thisToTrainQtyMax;

                                if (matchingMode.checked && unitToTrainQty >= 1) {
                                    break;
                                }
                            }
                        }
                    }
                }

                // Proceed with training if a unit is selected or if it's a Corral
                if (unitToTrain || hasClass(state, "Corral")) {
                    const queue = state.production.queue;
                    const canTrain = (queue.length === 0 ||
                        ((queue.length === 1 || state.production.autoqueue) && queue[0].timeRemaining <= 1600));

                    if (canTrain) {
                        let trainableUnits = this.splitTrainableUnitsAndCheck(unitToTrain, unitToTrainQty);

                        // Handle Corral-specific logic
                        if (hasClass(state, "Corral")) {
                            const unitCorralSelected = Engine.GetGUIObjectByName(`ControlPanelTabButton[1][5]_Name`).caption;
                            unitToTrain = this.getTrainableUnitsFromTrainer(state).find(x => x.includes(unitCorralSelected));

                            if (unitToTrain && Engine.GetGUIObjectByName(`ControlPanelTabButton[1][5]_Background`).sprite === this.toggledIcon) {
                                trainableUnits = this.getTrainableUnitsCorrals(unitToTrain);
                            } else {
                                continue;
                            }
                        }

                        // Train the units if available
                        if (trainableUnits >= 1) {
                            Engine.PostNetworkCommand({
                                type: "train",
                                entities: [this.playerEntities.Trainer[trainerID]],
                                template: unitToTrain,
                                count: trainableUnits,
                                pushFront: false
                            });

                            this.cacheStats(unitToTrain, trainableUnits, state);
                            this.hasBeenUpdated = { stats: false, pop: false };
                            this.moderngui_fullupdate_last = this.moderngui_statsupdate_last = this.gameTime;
                        }
                    }
                }
            }
        }
    }

    cacheStats(unit, batchSize, state) {
        let unitCost = GetTemplateData(unit).cost;
        if (hasClass(state, "Corral"))
            this.userState.classCounts.Animal += batchSize;    
        else if (hasClass(state, "Kennel"))
            this.userState.classCounts.Dog += batchSize;
        else
            this.userState.popCount += batchSize * unitCost.population;
        for (const resType of g_BoonGUIResTypes) {
            if (unitCost[resType] > 0)
                this.userState.resourceCounts[resType] -= unitCost[resType] * batchSize;
        }
        if (this.modes.find(x => x.name == unit))
            this.modes.find(x => x.name == unit).count += batchSize;
        this.idleTrainers--;
    }
    getPlayerEntities() {
        let playerEntities = {
            Trainer: [],
            Researcher: []
        };
        this.hasMarket = false;

        const processBuildingType = (buildingType) => {
            const { mode, entity } = buildingType;
            switch (mode) {
                case "Market":
                    this.hasMarket = true;
                    playerEntities["Trainer"].push(...entity);
                    break;
                case "Researcher":
                    playerEntities["Researcher"].push(...entity);
                    break;
                case "Trainer":
                    playerEntities["Trainer"].push(...entity);
                    break;
                case "Corral":
                    playerEntities["Trainer"].unshift(...entity);
                    break;
                default:
                    warn("Undefined Entity for trainer.");
            }

            for (const entityId of entity) {
                let state = GetEntityState(entityId);
                const queue = state?.production?.queue[0];
                if (queue) {
                    const unitTemplate = queue.unitTemplate;
                    if (unitTemplate) {
                        let unitTemplateInProduction = GetTemplateData(unitTemplate);
                        if (hasClass(state, "Corral"))
                            this.userState.classCounts.Animal += queue.count;
                        if (hasClass(state, "Kennel"))
                            this.userState.classCounts.Dog += queue.count;
                        const popCostUnitInProduction = unitTemplateInProduction?.cost?.population;
                        if (popCostUnitInProduction !== undefined) {
                            const count = queue.count * popCostUnitInProduction;
                            const trainerMatchingMode = this.modes.find(x => x.name === unitTemplate);
                            if (trainerMatchingMode) {
                                if (trainerMatchingMode.value === 0) {
                                    this.unitsToIgnore += count;
                                } else {
                                    trainerMatchingMode.count += count;
                                }
                            } else {
                                this.unitsToIgnore += count;
                            }
                        }
                    }
                }
            }
        };

        for (let buildingType of this.userState.productionBuildings) {
            processBuildingType(buildingType);
        }

        if (Array.isArray(this.userState.queue)) {
            const unitEntities = this.userState.queue.filter(item => item?.mode === "units");
            for (const entityType of unitEntities) {
                let templatedata = GetTemplateData(entityType.template);
                const popCostUnit = templatedata.cost.population;
                const matchingMode = this.modes.find(x => x.name === entityType.template);
                const count = entityType.count * popCostUnit;

                if (matchingMode) {
                    if (matchingMode.value === 0) {
                        this.unitsToIgnore += count;
                    } else {
                        matchingMode.count += count;
                    }
                } else {
                    this.unitsToIgnore += count;
                }
            }
        }

        return playerEntities;
    }

}

