class DiplomacyColors_CustomColors extends DiplomacyColors {

    constructor(...args)
    {
        super(...args);
    }

    updateDisplayedPlayerColors(...args)
    {
        if (!global.g_CustomColors)
            super.updateDisplayedPlayerColors(...args);
        else
        {
            this.computeTeamColors();

            Engine.GuiInterfaceCall("UpdateDisplayedPlayerColors", {
                "displayedPlayerColors": this.displayedPlayerColors,
                "displayDiplomacyColors": true, // This is changed
                "showAllStatusBars": g_ShowAllStatusBars,
                "selected": g_Selection.toList()
            });

            for (const handler of this.diplomacyColorsChangeHandlers)
	        handler(this.enabled);
        }
    }

}

DiplomacyColors = DiplomacyColors_CustomColors;
