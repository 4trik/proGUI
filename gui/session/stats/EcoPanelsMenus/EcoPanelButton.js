class EcoPanelButton {
    constructor(button, index, parent, mode) {
        this.button = button;
        this.index = index;
        this.parent = parent;
        this.mode = mode;

        this.bg = Engine.GetGUIObjectByName(`${button.name}_Background`);
        this.bgState = Engine.GetGUIObjectByName(`${button.name}_BackgroundState`);
        this.bgState.z = 20;
        this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/pause.png`;

        this.orderCount = Engine.GetGUIObjectByName(`${button.name}_Order`); //repurposed to store tech name
        this.orderCount.hidden = true;
        this.getSettings();

        this.isChecked = Engine.GetGUIObjectByName(`${button.name}`).checked;
        this.shouldBeHidden = !mode;

        if (mode) {
            this.initializeMode(mode);
        } else {
            button.hidden = true;
        }

        registerConfigChangeHandler(this.onConfigChange.bind(this));
    }

    initializeMode(mode) {
        const name = Engine.GetGUIObjectByName(`${this.button.name}_Name`);
        name.caption = mode.type;
        this.button.hidden = false;
        this.button.size = ProGUIGetColSize(this.index, 33);
        const icon = Engine.GetGUIObjectByName(`${this.button.name}_Icon`);

        icon.sprite = `stretched:session/portraits/technologies/${mode.icon}.png`;
        icon.size = "2 3 100%-2 100%-4";

        this.button.tooltip = colorizeHotkey(`${mode.title} %(hotkey)s`);
        this.button.onPress = () => {
            this.parent.toggleMode(this.index);
        };

        this.setupInitialCheckedState();
    }

    setupInitialCheckedState() {
        this.shouldBeHidden = false;
        this.isChecked = true;
    }

    onConfigChange(changes) {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("moderngui.trainer"))) {
            this.getSettings();
        }
        this.bg.sprite = this.bg.sprite === "stretched:session/icons/bkg/portrait_black.dds" ? "stretched:session/icons/bkg/portrait_black.dds" : this.toggledIcon;
    }

    getSettings() {
        this.toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "moderngui.trainer.color") + ".dds";
    }

    update(toToggleIndex, menuID) {
        if (toToggleIndex === this.index) {
            this.isChecked = !this.isChecked;
        }
        this.bgState.sprite = Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.8`) === "true" ? "stretched:color:dimmedWhite:textureAsMask:session/phosphor/pause.png" : this.bgState.sprite;
        this.bg.sprite = this.isChecked ? this.toggledIcon : `stretched:session/icons/bkg/portrait_black.dds`;
        this.bgState.hidden = this.isChecked ? Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.${menuID}`) !== "false" : false;
        Engine.GetGUIObjectByName(`${this.button.name}`).checked = this.isChecked;
    }

    hide(boolean) {
        if (!this.shouldBeHidden) {
            this.button.hidden = boolean;
        }
    }
}
