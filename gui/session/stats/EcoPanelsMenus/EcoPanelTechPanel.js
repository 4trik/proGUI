class EcoPanelTechPanel extends EcoPanel {
    static Modes = [
        { "type": "Melee", "icon": "sword", "title": "Yes/No upgrade melee weapons", "tech":  "soldier_attack_melee" },
        { "type": "Ranged", "icon": "arrow", "title": "Yes/No upgrade ranged weapons", "tech":  "soldier_attack_ranged" },
        { "type": "Hack Resistance", "icon": "armor_leather", "title": "Yes/No upgrade body armor", "tech":  "soldier_resistance_hack" },
        { "type": "Pierce Resistance", "icon": "shields_generic_wood", "title": "Yes/No upgrade shields", "tech": "soldier_resistance_pierce" },
    ];

    constructor(forceRender) {
        const PREFIX = "ControlPanel";
        super(forceRender, PREFIX, EcoPanelTechPanel.Modes);

        if (!g_IsObserver) {
            this.panelButtonsName = `${PREFIX}TabButton[2]`;
            this.panelButtons = Engine.GetGUIObjectByName(`${PREFIX}TabButtons[2]`);
            this.buttons = this.panelButtons.children.map((button, index) => new EcoPanelTechPanelButton(button, index, this));
            this.getSettings();
            this.setMenuAspect();
            this.buttons.forEach(button => button.update(-1));
            registerConfigChangeHandler(this.onConfigChange.bind(this));
        }
    }
    setMenuAspect() {
        super.setMenuAspect();
        if (this.stackedSubMenus) {
            this.panelButtons.size = "100%-133 -83 37 -48";
            this.areButtonsHidden = false;
            this.buttons.forEach(button => button.hide(this.areButtonsHidden));
        } else {
            this.panelButtons.size = "100%-133 -47 37 -12";
        }
    }
}
