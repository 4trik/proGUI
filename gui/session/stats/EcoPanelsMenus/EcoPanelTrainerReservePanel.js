class EcoPanelTrainerReservePanel extends EcoPanel {
    static Modes = [
        { "type": "Food", "icon": "food", "title": "CLICK: Yes/No food reserve. \nMOUSEWHEEL: change reserve levels" },
        { "type": "Wood", "icon": "wood", "title": "CLICK: Yes/No food reserve. \nMOUSEWHEEL: change reserve levels" },
        { "type": "Stone", "icon": "stone", "title": "CLICK: Yes/No stone reserve. \nMOUSEWHEEL: change reserve levels" },
        { "type": "Metal", "icon": "metal", "title": "CLICK: Yes/No metal reserve. \nMOUSEWHEEL: change reserve levels" },
        { "type": "Population", "icon": "population", "title": "MOUSEWHEEL: change max batch size \nCLICK: Stop Training non-animal units. \nRIGHT CLICK: Set Max Batch to 1." },
        { "type": "Corral", "icon": "population", "title": "MOUSEWHEEL: change max batch size \nCLICK: On/Off Corral. \nRIGHT CLICK: Change unit." }
    ];

    constructor(forceRender) {
        const PREFIX = "ControlPanel";
        super(forceRender, PREFIX, EcoPanelTrainerReservePanel.Modes);
        if (!g_IsObserver) {
            this.panelButtonsName = `${PREFIX}TabButton[1]`;
            this.panelButtons = Engine.GetGUIObjectByName(`${PREFIX}TabButtons[1]`);
            this.buttons = this.panelButtons.children.map((button, index) => new EcoPanelTrainerReservePanelButton(button, index, this));
            this.panelButtons.size = "100%-200 -47 37 -12";
            this.enabledCorral = Engine.ConfigDB_GetValue("user", "moderngui.trainer.enableCorral") == "true" ? 5 : -1;
            this.getSettings();
            this.setMenuAspect();
            this.buttons.forEach(button => button.update(-1));
            registerConfigChangeHandler(this.onConfigChange.bind(this));
        }
    }

    setMenuAspect() {
        super.setMenuAspect();
        if (this.stackedSubMenus) {
            this.areButtonsHidden = false;
            this.buttons.forEach(button => button.hide(this.areButtonsHidden));
        }
    }
}
