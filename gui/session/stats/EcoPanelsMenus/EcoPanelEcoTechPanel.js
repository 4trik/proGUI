class EcoPanelEcoTechPanel extends EcoPanel {
    static Modes = [
        { "type": "Wood-Cutter", "icon": "wood_axe", "title": "Yes/No Upgrade Wood-Cutter's axes", "tech":  "gather_lumbering" },
        { "type": "Farming", "icon": "farming_training", "title": "Yes/No Upgrade Plows", "tech": "gather_farming" },
        { "type": "Stone-cutter", "icon": "mining_stone", "title": "Yes/No Upgrade Stone-Cutter's tools", "tech":  "gather_mining" },
        { "type": "Metal-mining", "icon": "mining_metal", "title": "Yes/No Upgrade Miner's tools", "tech":  "gather_mining" },
        { "type": "Fruit-gathering", "icon": "gather_basket", "title": "Yes/No Upgrade Gatherer's tools", "tech":  "gather_wicker" }
    ];

    constructor(forceRender) {
        const PREFIX = "ControlPanel";
        super(forceRender, PREFIX, EcoPanelEcoTechPanel.Modes);
        if (!g_IsObserver) {
            this.panelButtonsName = `${PREFIX}TabButton[3]`;
            this.panelButtons = Engine.GetGUIObjectByName(`${PREFIX}TabButtons[3]`);
            this.buttons = this.panelButtons.children.map((button, index) => new EcoPanelEcoTechPanelButton(button, index, this));
            this.getSettings();
            this.setMenuAspect();
            this.buttons.forEach(button => button.update(-1));
            registerConfigChangeHandler(this.onConfigChange.bind(this));
        }
    }

    setMenuAspect() {
        super.setMenuAspect();
        if (this.stackedSubMenus) {
            this.panelButtons.size = "100%-166 -119 37 -84";
            this.areButtonsHidden = false;
            this.buttons.forEach(button => button.hide(this.areButtonsHidden));
        } else {
            this.panelButtons.size = "100%-166 -47 37 -12";
        }
    }
}
