class EcoPanelMasterPanel extends EcoPanel {
    static Modes = [
        { "type": "Training", "icon": "users-three", "title": "On/Off Training \nMOUSE-OVER for settings" },
        { "type": "Military", "icon": "sword", "title": "On/Off Military Researchs \nMOUSE-OVER for settings" },
        { "type": "Eco", "icon": "axe", "title": "On/Off Eco Researchs \nMOUSE-OVER for settings" },
        { "type": "Tribute", "icon": "exchange", "title": "On/Off Tribute Allies \nMOUSE-OVER for settings" },
        { "type": "Food", "icon": "food", "title": "Market price" },
        { "type": "Wood", "icon": "wood", "title": "Market price" },
        { "type": "Stone", "icon": "stone", "title": "Market price" },
        { "type": "Metal", "icon": "metal", "title": "Market price" },
        { "type": "Phase", "icon": "upgrade", "title": "PAUSE all until next phase" }
    ];

    constructor(forceRender) {
        const PREFIX = "ControlPanel";
        super(forceRender, PREFIX, EcoPanelMasterPanel.Modes);

        if (!g_IsObserver) {
            this.getSettings();
            this.panelButtonsName = `${PREFIX}TabButton[0]`;
            this.panelButtons = Engine.GetGUIObjectByName(`${PREFIX}TabButtons[0]`);
            this.buttons = this.panelButtons.children.map((button, index) => new EcoPanelMasterPanelButton(button, index, this));
            this.setMenuAspect();
            this.isPaused = false;
            this.buttons.forEach(button => button.update(-1, this.isPaused));
            registerConfigChangeHandler(this.onConfigChange.bind(this));
        }
    }

    getSettings() {
        super.getSettings();
        this.barterIsEnabled = Engine.ConfigDB_GetValue("user", "moderngui.trainer.barter.isEnabled") == "true";
    }

    setMenuAspect() {
        //super.setMenuAspect(); Doesn't apply to master panel.
        if (this.barterIsEnabled) {
            this.panelButtons.size = "100%-300 0 37 37";
        } else {
            this.panelButtons.size = "100%-135 0 37 37";
        }
    }

    toggleMode(modeIndex, endPause) {
        if (!g_IsObserver) {
            const mode = EcoPanelMasterPanel.Modes[modeIndex];
            if (!mode) return;
            let isModeToggled = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][${modeIndex}]`).checked;
            if (!isModeToggled) {
                if (mode.type == "Phase") {
                    this.isPaused = false;
                }
            } else {
                if (mode.type == "Phase") {
                    this.isPaused = true;
                }
            }
            if (endPause == true) {
                this.isPaused = false;
                Engine.GetGUIObjectByName(`ControlPanelTabButton[0][8]`).checked = false; // Assuming Phase is at index 8
            }
            this.buttons.forEach(button => button.update(modeIndex, this.isPaused));
            this.forceRender();
        }
    }
}
