class EcoPanelEcoTechPanelButton extends EcoPanelButton {
    constructor(button, index, parent) {
        const mode = EcoPanelEcoTechPanel.Modes[index] ?? null;
        super(button, index, parent, mode);
        this.bg.sprite = this.toggledIcon;
        this.techTier = Engine.GetGUIObjectByName(`${button.name}_Count`);
        this.techTier.caption = "III";
        this.techTier.font = "mono-10";
        this.techTier.hidden = true;
        if (mode) {
            if (index === 0 || index === 2 || index === 3) { //Only Storehouse techs can detect tiers
                this.button.onMouseWheelUp = () => {
                    this.increaseReserve();
                };
                this.button.onMouseWheelDown = () => {
                    this.decreaseReserve();
                };
            }
            this.orderCount.caption = mode.tech;
            this.modeIcon = mode.icon;
            this.orderCount.caption = mode.tech;
            this.setupInitialCheckedState();
        } else {
            this.shouldBeHidden = true;
        }
        this.button.hidden = true;
    }
    normalizeReserve() {
        if (this.techTier.caption.length > 3) {
            this.techTier.caption = "III";
        } else if (this.techTier.caption.length < 1) {
            this.techTier.caption = "";
            if (this.isChecked) {
                this.update(this.index);
            }
            this.techTier.hidden = true;
        }
    }

    increaseReserve() {
        this.techTier.caption += "I";
        this.techTier.hidden = false;
        if (!this.isChecked) {
            this.update(this.index);
        }
        this.normalizeReserve();
    }

    decreaseReserve() {
        this.techTier.caption = this.techTier.caption.slice(0, -1);
        this.techTier.hidden = false;
        this.normalizeReserve();
    }

    setupInitialCheckedState() {
        switch (this.index) {
            case 0:
                this.isChecked = (Engine.ConfigDB_GetValue("user", "moderngui.trainer.techPanel.storeHouseActive") === "true");
                break;
            case 1:
                this.isChecked = (Engine.ConfigDB_GetValue("user", "moderngui.trainer.techPanel.farmActive") === "true");
                break;
            case 4:
                this.isChecked = (Engine.ConfigDB_GetValue("user", "moderngui.trainer.techPanel.fruitsActive") === "true");
                break;
            default:
                this.isChecked = false;
        }
    }

    update(toToggleIndex) {
        let menuID = 2;
        super.update(toToggleIndex, menuID);
    }
}
