class EcoPanelTrainerReservePanelButton extends EcoPanelButton {
    constructor(button, index, parent) {
        const mode = EcoPanelTrainerReservePanel.Modes[index] ?? null;
        super(button, index, parent, mode);

        this.reserveLevel = Engine.GetGUIObjectByName(`${button.name}_Count`);
        const name = Engine.GetGUIObjectByName(`${button.name}_Name`);
        this.bg = Engine.GetGUIObjectByName(`${button.name}_Background`);
        this.bgState = Engine.GetGUIObjectByName(`${button.name}_BackgroundState`);
        this.bgState.z = 20;
        this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/pause.png`;
        this.getSettings();
        this.bg.sprite = this.toggledIcon;
        this.corralIcons = ["goat", "sheep", "pig", "cattle"];
        this.corralIconSelected = 0;

        this.isChecked = Engine.GetGUIObjectByName(`${button.name}`).checked;
        if (mode) {
            const icon = Engine.GetGUIObjectByName(`${button.name}_Icon`);
            switch (this.index) {
                case 4:
                    icon.sprite = `stretched:session/icons/resources/${mode.icon}.png`;
                    this.reserveLevel.caption = Math.floor(Engine.ConfigDB_GetValue("user", `moderngui.trainer.MaxBatchSize`));
                    name.caption = mode.type;
                    button.onMouseRightPress = () => {
                        this.reserveLevel.caption = 1;
                    };
                    break;
                case 5:
                    this.reserveLevel.caption = Math.floor(Engine.ConfigDB_GetValue("user", `moderngui.trainer.corralBatchSize`));
                    icon.sprite = `stretched:session/icons/${this.corralIcons[this.corralIconSelected]}.png`;
                    name.caption = this.corralIcons[this.corralIconSelected];
                    button.onMouseRightPress = () => {
                        this.corralIconSelected = (this.corralIconSelected + 1) % 4;
                        icon.sprite = `stretched:session/icons/${this.corralIcons[this.corralIconSelected]}.png`;
                        name.caption = this.corralIcons[this.corralIconSelected];
                    };
                    if (Engine.ConfigDB_GetValue("user", "moderngui.trainer.enableCorral") != "true") {
                        this.isChecked = false;
                        this.bg.sprite = `stretched:session/icons/bkg/portrait_black.dds`;
                    }
                    break;
                default:
                    this.reserveLevel.caption = Math.floor(Engine.ConfigDB_GetValue("user", `moderngui.trainer.${mode.icon}Reserve`));
                    icon.sprite = `stretched:session/icons/resources/${mode.icon}.png`;
                    name.caption = mode.type;
            }
            button.hidden = false;
            button.size = ProGUIGetColSize(index, 33);

            button.tooltip = colorizeHotkey(`${mode.title}`);
            button.onMouseWheelUp = () => {
                this.increaseReserve(index);
            };
            button.onMouseWheelDown = () => {
                this.decreaseReserve(index);
            };

            this.shouldBeHidden = false;
        } else {
            this.shouldBeHidden = true;
        }
        button.hidden = true;
        registerConfigChangeHandler(this.onConfigChange.bind(this));
    }

    onConfigChange(changes) {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("moderngui.trainer")))
            this.getSettings();
        this.bg.sprite = this.bg.sprite == "stretched:session/icons/bkg/portrait_black.dds" ? "stretched:session/icons/bkg/portrait_black.dds" : this.toggledIcon;
    }

    getSettings() {
        this.toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "moderngui.trainer.color") + ".dds";
    }

    normalizeReserve() {
        if (this.reserveLevel.caption * 1 > 1000)
            this.reserveLevel.caption = 1000;
        else if (this.reserveLevel.caption * 1 < 0)
            this.reserveLevel.caption = 0;
    }

    increaseReserve() {
        if (this.index == 4 || this.index == 5)
            this.reserveLevel.caption = this.reserveLevel.caption * 1 + 1;
        else
            if (this.reserveLevel.caption < 200)
				this.reserveLevel.caption = this.reserveLevel.caption * 1 + 25;
            else if (this.reserveLevel.caption < 500)
                this.reserveLevel.caption = this.reserveLevel.caption * 1 + 50;
            else
                this.reserveLevel.caption = this.reserveLevel.caption * 1 + 100;
        this.normalizeReserve();
    }

    decreaseReserve() {
        if (this.index == 4 || this.index == 5)
            this.reserveLevel.caption = this.reserveLevel.caption * 1 - 1;
        else
            if (this.reserveLevel.caption < 200)
                this.reserveLevel.caption = this.reserveLevel.caption * 1 - 25;
            else if (this.reserveLevel.caption < 500)
                this.reserveLevel.caption = this.reserveLevel.caption * 1 - 50;
            else
                this.reserveLevel.caption = this.reserveLevel.caption * 1 - 100;
        this.normalizeReserve();
    }

    update(toToggleIndex) {
        let menuID = 0;
        super.update(toToggleIndex, menuID);
    }

    hide(boolean) {
        super.hide(boolean);
    }
}