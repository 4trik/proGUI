class EcoPanelTechPanelButton extends EcoPanelButton {
    constructor(button, index, parent) {
        const mode = EcoPanelTechPanel.Modes[index] ?? null;
        super(button, index, parent, mode);
        this.techTier = Engine.GetGUIObjectByName(`${button.name}_Count`);
        this.techTier.caption = "III";
        this.techTier.font = "mono-10";
        this.techTier.hidden = true;
        if (mode) {
            this.button.onMouseWheelUp = () => {
                this.increaseReserve();
            };
            this.button.onMouseWheelDown = () => {
                this.decreaseReserve();
            };
            this.orderCount.caption = mode.tech;
        }
        this.button.hidden = true;
    }
    normalizeReserve() {
        if (this.techTier.caption.length > 3) {
            this.techTier.caption = "III";
        } else if (this.techTier.caption.length < 1) {
            this.techTier.caption = "";
            if (this.isChecked) {
                this.update(this.index);
            }
            this.techTier.hidden = true;
        }
    }

    increaseReserve() {
        this.techTier.caption += "I";
        this.techTier.hidden = false;
        if (!this.isChecked) {
            this.update(this.index);
        }
        this.normalizeReserve();
    }

    decreaseReserve() {
        this.techTier.caption = this.techTier.caption.slice(0, -1);
        this.techTier.hidden = false;
        this.normalizeReserve();
    }
    update(toToggleIndex) {
        let menuID = 1;
        super.update(toToggleIndex, menuID);
    }
}
