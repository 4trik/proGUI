class EcoPanelTributePanelButton extends EcoPanelButton {
    constructor(button, index, parent) {
        const mode = EcoPanelTributePanel.Modes[index] ?? null;
        super(button, index, parent, mode); // Call the parent constructor

        this.reserveLevel = Engine.GetGUIObjectByName(`${button.name}_Count`);
        const name = Engine.GetGUIObjectByName(`${button.name}_Name`);
        this.bgState.z = 20;
        this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/pause.png`;
        this.getSettings();
        this.bg.sprite = this.toggledIcon;

        this.isChecked = Engine.GetGUIObjectByName(`${button.name}`).checked;
        if (mode) {
            this.reserveLevel.caption = Math.round(Engine.ConfigDB_GetValue("user", `moderngui.trainer.autotribute.tributeShare${mode.type}`) * 100) / 100;
            name.caption = mode.type;
            button.hidden = false;
            button.size = ProGUIGetColSize(index, 33);
            const icon = Engine.GetGUIObjectByName(`${button.name}_Icon`);

            icon.sprite = `stretched:session/icons/resources/${mode.icon}.png`;

            button.tooltip = colorizeHotkey(`${mode.title}`);
            button.onMouseWheelUp = () => {
                this.increaseReserve();
            };
            button.onMouseWheelDown = () => {
                this.decreaseReserve();
            };
            this.isChecked = true;
            this.shouldBeHidden = false;
        } else {
            this.shouldBeHidden = true;
        }
        button.hidden = true;
        registerConfigChangeHandler(this.onConfigChange.bind(this));
    }

    onConfigChange(changes) {
        super.onConfigChange(changes); // Call the parent method
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("moderngui.trainer"))) {
            this.getSettings();
        }
        this.bg.sprite = this.bg.sprite === "stretched:session/icons/bkg/portrait_black.dds" ? "stretched:session/icons/bkg/portrait_black.dds" : this.toggledIcon;
    }

    getSettings() {
        super.getSettings(); // Call the parent method
    }

    normalizeReserve() {
        if (this.reserveLevel.caption * 1 > 1.2)
            this.reserveLevel.caption = 1.2;
        else if (this.reserveLevel.caption * 1 < 0)
            this.reserveLevel.caption = 0;
    }

    increaseReserve() {
        let newValue = (this.reserveLevel.caption * 1 + 0.02);
        this.reserveLevel.caption = Math.round(newValue * 100) / 100;
        this.normalizeReserve();
    }

    decreaseReserve() {
        this.reserveLevel.caption = Math.round((this.reserveLevel.caption * 100 - 2)) / 100;
        this.normalizeReserve();
    }

    update(toToggleIndex) {
        let menuID = 3;
        super.update(toToggleIndex, menuID);
    }

    hide(boolean) {
        super.hide(boolean);
    }
}
