class EcoPanelMasterPanelButton {
	constructor(button, index, parent) {
		this.mode = EcoPanelMasterPanel.Modes[index] ?? null;
		this.button = button;
		this.index = index;
		this.parent = parent;

		this.sellPrice = Engine.GetGUIObjectByName(`${button.name}_Count`);
		this.orderCount = Engine.GetGUIObjectByName(`${button.name}_Order`);
		this.name = Engine.GetGUIObjectByName(`${button.name}_Name`);
		this.bg = Engine.GetGUIObjectByName(`${button.name}_Background`);
		this.bgState = Engine.GetGUIObjectByName(`${button.name}_BackgroundState`);
		this.getSettings();
		const toggledIcon = this.toggledIcon;
		this.bg.sprite = toggledIcon;
		this.orderCount.caption = "▲";
		this.orderCount.font
		this.orderCount.hidden = true;
		this.switchSubMenuCooldownTime = 260;
		this.switchSubMenuCooldownTimeMax = 260; //Unedited value to reset to. The normal value can be increased when button is disabled for example
		this.mouseLeft = false;
		this.isChecked = Engine.GetGUIObjectByName(`${button.name}`).checked;
		this.setMenuAspect();
		registerConfigChangeHandler(this.onConfigChange.bind(this));

	}
	onConfigChange(changes) {
		const changesList = [...changes];
		if (changesList.some(x => x.startsWith("moderngui.trainer"))) {
			this.getSettings();
			this.setMenuAspect();
		}
	}
	getSettings() {
		this.toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "moderngui.trainer.color") + ".dds";
		this.stackedSubMenu = Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.showAllMenus`);
	}
	setMenuAspect() {
		this.bg.sprite = this.bg.sprite == "stretched:session/icons/bkg/portrait_black.dds" ? "stretched:session/icons/bkg/portrait_black.dds" : this.toggledIcon;
		if (this.mode) {
			this.name.caption = this.mode.type;
			this.button.hidden = false;
			this.button.size = ProGUIGetColSize(this.index, 33);

			const icon = Engine.GetGUIObjectByName(`${this.button.name}_Icon`);

			if (this.index > 3 && this.index != 8) {
				if (Engine.ConfigDB_GetValue("user", "moderngui.trainer.barter.isEnabled") == "true") {
					icon.sprite = `stretched:session/icons/resources/${this.mode.icon}.png`;
				}
				else
					this.button.hidden = true;
				this.button.enabled = false;
				this.bgState.hidden = true;
			}

			else
				icon.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/${this.mode.icon}.png`;

			this.button.tooltip = colorizeHotkey(`${this.mode.title} %(hotkey)s`, `moderngui.session.controlPanel.${this.index + 1}`);
			this.button.onPress = () => {
				this.parent.toggleMode(this.index);
			};
			if (this.index > 3)
				this.isChecked = false;
			else if (Engine.ConfigDB_GetValue("user", "moderngui.trainer.controlPanel.preactivateHelpers") === "true") {
				this.isChecked = (Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.${this.index}`) === "true") ? true : false;
			}
			else {
				if (Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.${this.index}`) === "true")
					toggleConfigBool(`moderngui.trainer.controlPanel.${this.index}`);
				this.isChecked = false;
			}
			if (!this.isChecked)
				this.bg.sprite = `stretched:session/icons/bkg/portrait_black.dds`;
		}
		if (this.stackedSubMenu == "true") {
			this.button.onMouseEnter = () => { };
			this.button.onMouseLeave = () => { };
		}
		//Define settings options for certain buttons
		switch (this.index * 1) {
			case 0: //Trainer Toggle
				if (this.stackedSubMenu != "true") {
					this.button.onMouseEnter = async () => {
						this.mouseLeft = false;
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
						if (!this.mouseLeft) {
							this.orderCount.hidden = false;
							g_stats.reservePanel.hideShowButtons(false);
							g_stats.techPanel.hideShowButtons(true);
							g_stats.ecoTechPanel.hideShowButtons(true);
							g_stats.tributePanel.hideShowButtons(true);

						}
					}
					this.button.onMouseLeave = () => {
						this.mouseLeft = true;
						this.orderCount.hidden = true;
					}
					this.button.onMouseRightPress = async () => {
						g_stats.tributePanel.hideShowButtons(true);
						g_stats.reservePanel.hideShowButtons(true);
						g_stats.techPanel.hideShowButtons(true);
						g_stats.ecoTechPanel.hideShowButtons(true);
						this.mouseLeft = false;
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
						this.mouseLeft = true;
					};
				}
				break;
			case 1: //Tech Toggle
				if (this.stackedSubMenu != "true") {
					this.button.onMouseEnter = async () => {
						this.mouseLeft = false;
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
						if (!this.mouseLeft) {
							this.orderCount.hidden = false;
							g_stats.techPanel.hideShowButtons(false);
							g_stats.reservePanel.hideShowButtons(true);
							g_stats.ecoTechPanel.hideShowButtons(true);
							g_stats.tributePanel.hideShowButtons(true);
						}
					}
					this.button.onMouseLeave = () => {
						this.mouseLeft = true;
						this.orderCount.hidden = true;
					}
					this.button.onMouseRightPress = async () => {
						g_stats.tributePanel.hideShowButtons(true);
						g_stats.reservePanel.hideShowButtons(true);
						g_stats.techPanel.hideShowButtons(true);
						g_stats.ecoTechPanel.hideShowButtons(true);
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
					};
				}
				break;
			case 2: //Eco Tech Toggle
				if (this.stackedSubMenu != "true") {
					this.button.onMouseEnter = async () => {
						this.mouseLeft = false;
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
						if (!this.mouseLeft) {
							this.orderCount.hidden = false;
							g_stats.ecoTechPanel.hideShowButtons(false);
							g_stats.reservePanel.hideShowButtons(true);
							g_stats.techPanel.hideShowButtons(true);
							g_stats.tributePanel.hideShowButtons(true);
						}
					}
					this.button.onMouseLeave = () => {
						this.mouseLeft = true;
						this.orderCount.hidden = true;
					}
					this.button.onMouseRightPress = async () => {
						g_stats.tributePanel.hideShowButtons(true);
						g_stats.reservePanel.hideShowButtons(true);
						g_stats.techPanel.hideShowButtons(true);
						g_stats.ecoTechPanel.hideShowButtons(true);
						this.mouseLeft = false;
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
						this.mouseLeft = true;
					};
				}
				break;
			case 3: //Tribute Toggle
				if (this.stackedSubMenu == "true") {//Specific for stacked menu because the tribute is hidden
					this.button.onMouseRightPress = () => {
						g_stats.tributePanel.hideShowButtons();
						this.orderCount.hidden = false;
					}
					this.button.onMouseLeave = () => {
						this.mouseLeft = true;
						this.orderCount.hidden = true;
					}
				}
				else
					this.button.onMouseRightPress = async () => {
						g_stats.tributePanel.hideShowButtons(true);
						g_stats.reservePanel.hideShowButtons(true);
						g_stats.techPanel.hideShowButtons(true);
						g_stats.ecoTechPanel.hideShowButtons(true);
						this.mouseLeft = false;
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
						this.mouseLeft = true;
					};
				if (this.stackedSubMenu != "true") {
					this.button.onMouseEnter = async () => {
						this.mouseLeft = false;
						await new Promise(resolve => setTimeout(resolve, this.switchSubMenuCooldownTime));
						if (!this.mouseLeft) {
							this.orderCount.hidden = false;
							g_stats.tributePanel.hideShowButtons(false);
							g_stats.reservePanel.hideShowButtons(true);
							g_stats.techPanel.hideShowButtons(true);
							g_stats.ecoTechPanel.hideShowButtons(true);
						}
					}
					this.button.onMouseLeave = () => {
						this.mouseLeft = true;
						this.orderCount.hidden = true;
					}
				}
				break;
			default:
				this.button.onMouseRightPress = () => {
					g_stats.tributePanel.hideShowButtons(true);
					g_stats.reservePanel.hideShowButtons(true);
					g_stats.techPanel.hideShowButtons(true);
					g_stats.ecoTechPanel.hideShowButtons(true);
				};
		}
	}
	update(toToggleIndex, isPaused) {

		if (toToggleIndex === this.index) {

			this.isChecked = !this.isChecked;
			if (this.index <= 3)
				toggleConfigBool(`moderngui.trainer.controlPanel.${this.index}`);
		}
		if (this.isChecked === true) {
			const toggledIcon = this.toggledIcon;
			this.bg.sprite = toggledIcon;
			this.switchSubMenuCooldownTime = this.switchSubMenuCooldownTimeMax;
		}
		else if (this.isChecked === false) {
			this.bg.sprite = `stretched:session/icons/bkg/portrait_black.dds`;
			this.switchSubMenuCooldownTime = this.switchSubMenuCooldownTime * 3;
		}


		if ((isPaused && this.index != 8) || this.isChecked === false) {
			this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/pause.png`;
			for (let i = 0; i < 9; i++) {
				const toPauseButtons = Engine.GetGUIObjectByName(`ControlPanelTabButton[${this.index + 1}][${i}]_BackgroundState`);
				if (toPauseButtons != null) {
					toPauseButtons.hidden = false;
					toPauseButtons.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/pause.png`;
				}
			}
		}
		else if ((this.isChecked === true && !isPaused)) {
			this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/play.png`;
			for (let i = 0; i < 9; i++) {
				const toPauseButtons = Engine.GetGUIObjectByName(`ControlPanelTabButton[${this.index + 1}][${i}]_BackgroundState`);
				const toggledIcon = this.toggledIcon;
				if (toPauseButtons != null && Engine.GetGUIObjectByName(`ControlPanelTabButton[${this.index + 1}][${i}]_Background`).sprite == toggledIcon) {
					toPauseButtons.hidden = true;
				}
			}
		}
		if (this.index == 8 && this.isChecked)
			this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/play.png`;
		Engine.GetGUIObjectByName(`${this.button.name}`).checked = this.isChecked;

	}
}
