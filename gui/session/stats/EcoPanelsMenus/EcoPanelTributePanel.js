class EcoPanelTributePanel extends EcoPanel {
    static Modes = [
        { "type": "Food", "icon": "food", "title": "CLICK: Yes/No share food. \nMOUSEWHEEL: change share levels" },
        { "type": "Wood", "icon": "wood", "title": "CLICK: Yes/No share wood. \nMOUSEWHEEL: change share levels" },
        { "type": "Stone", "icon": "stone", "title": "CLICK: Yes/No share stone. \nMOUSEWHEEL: change share levels" },
        { "type": "Metal", "icon": "metal", "title": "CLICK: Yes/No share metal. \nMOUSEWHEEL: change share levels" },
    ];

    constructor(forceRender) {
        const PREFIX = "ControlPanel";
        super(forceRender, PREFIX, EcoPanelTributePanel.Modes);

        if (!g_IsObserver) {
            this.panelButtonsName = `${PREFIX}TabButton[4]`;
            this.panelButtons = Engine.GetGUIObjectByName(`${PREFIX}TabButtons[4]`);
            this.buttons = this.panelButtons.children.map((button, index) => new EcoPanelTributePanelButton(button, index, this));
            this.getSettings();
            this.setMenuAspect();
            this.buttons.forEach(button => button.update(-1));
            registerConfigChangeHandler(this.onConfigChange.bind(this));
        }
    }
    setMenuAspect() {
        super.setMenuAspect();
        if (this.stackedSubMenus) {
            this.panelButtons.size = "100%-135 -155 37 -120";
            this.areButtonsHidden = true;
            this.buttons.forEach(button => button.hide(this.areButtonsHidden));
        } else {
            this.panelButtons.size = "100%-135 -47 37 -12";
        }
    }
}
