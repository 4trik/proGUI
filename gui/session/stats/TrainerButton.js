class TrainerButton {
	constructor(tab, index, parent, doPreset) {
		const mode = Trainer.Modes[index] ?? null;
		this.tab = tab;
		this.index = index;

		this.toProduce = Engine.GetGUIObjectByName(`${tab.name}_Count`);
		this.symbol = Engine.GetGUIObjectByName(`${tab.name}_Symbol`);
		this.name = Engine.GetGUIObjectByName(`${tab.name}_Name`);
		this.toProduce.caption = 0;
		this.bg = Engine.GetGUIObjectByName(`${tab.name}_Background`);
		this.isChecked = Engine.GetGUIObjectByName(`${tab.name}`).checked;
		this.isChecked = false;
		this.doPreset = doPreset;
		this.getSettings();
		this.setMenuAspect();
		this.bg.sprite = `stretched:session/icons/bkg/portrait_black.dds`;

		if (mode) {
			this.name.caption = mode.type;
			tab.hidden = false;
			tab.size = ProGUIGetRowSize(index, 46);
			const text = Engine.GetGUIObjectByName(`${tab.name}_Text`);
			const icon = Engine.GetGUIObjectByName(`${tab.name}_Icon`);

			icon.sprite = `stretched:session/portraits/${mode.icon}`;

			tab.tooltip = colorizeHotkey(`${mode.title} %(hotkey)s`, `moderngui.session.stats.mode.${index + 1}`);
			tab.tooltip += ' Use MOUSEWHEEL to change value, CLICK to toggle';
			tab.onPress = () => {
				parent.toggleMode(index);
			};
			tab.onPressRight = () => {
				if (mode.type == null) return;
				showTemplateDetails(mode.type);
			}
		}
		registerConfigChangeHandler(this.onConfigChange.bind(this));
	}
	onConfigChange(changes) {
		const changesList = [...changes];
		if (changesList.some(x => x.startsWith("moderngui.trainer")))
			this.getSettings();
		this.bg.sprite = this.bg.sprite == "stretched:session/icons/bkg/portrait_black.dds" ? "stretched:session/icons/bkg/portrait_black.dds" : this.toggledIcon;
	}
	getSettings() {
		this.toogleResetToZero = Engine.ConfigDB_GetValue("user", "moderngui.trainer.toogleResetToZero") == "true";
		this.toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "moderngui.trainer.color") + ".dds";
	}
	setMenuAspect() {
		if (this.doPreset) {
			if (this.index == 1 && !this.toogleResetToZero) {
				this.toProduce.caption = 50;
				this.symbol.caption = "%";
			}
			else if (this.index == 2 && !this.toogleResetToZero) {
				this.toProduce.caption = 50;
				this.symbol.caption = "%";
			}
			else
				this.symbol.caption = "%";
			this.doPreset = false;
		}
	}
	update(toToggleIndex) {
		if (toToggleIndex === this.index && this.isChecked === false) {
			const toggledIcon = this.toggledIcon;
			this.bg.sprite = toggledIcon;
			this.isChecked = true;
			this.symbol.caption = "∞";
			if (!this.toogleResetToZero) {
				this.toProduce.font = "sans-bold-stroke-16";
			}
			else {
				this.toProduce.hidden = true;
				this.toProduce.caption = 0;
			}
			this.symbol.font = "sans-bold-stroke-20";
		}
		else if (toToggleIndex === this.index && this.isChecked === true) {
			this.bg.sprite = `stretched:session/icons/bkg/portrait_black.dds`;
			this.isChecked = false;
			this.symbol.caption = "%";
			this.toProduce.hidden = false;
			this.toProduce.font = "sans-bold-stroke-18";
			this.symbol.font = "sans-bold-stroke-16";
		}
		if (this.toogleResetToZero && this.symbol.caption == "∞")
			this.symbol.text_valign = "center";
		else
			this.symbol.text_valign = "bottom";
		if (this.isChecked === true) { //Something is assigning the wrong symbol to new tabs sometimes.
			this.symbol.caption = "∞";
			this.toProduce.font = "sans-bold-stroke-16";
		}
		else {
			this.symbol.caption = "%";
			this.symbol.font = "sans-bold-stroke-16";
		}
	}

}
