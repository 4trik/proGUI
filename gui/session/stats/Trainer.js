class Trainer {


	constructor(forceRender) {
		this.playersStates = this.getPlayersStates();
		this.userState = this.playersStates.find(x => x.index === g_ViewedPlayer);
		this.trainers = this.getPlayerEntities();
		this.trainableUnits = this.getPlayerTrainableUnits();
		this.toggledModes = new Array();
		// Store the initial order of the tabs
		this.initialTabOrder = [];
		this.needToRefreshTabs = true;

		Object.defineProperty(Trainer, "Modes", {
			value: [],
			writable: true
		});

		for (let unit of this.trainableUnits) {
			const template = GetTemplateData(unit);
			Trainer.Modes.push(
				{ "type": unit, "icon": template.icon, "title": template.name.generic }
			);
		}

		this.getSettings();
		const PREFIX = "StatsModes";
		this.root = Engine.GetGUIObjectByName(PREFIX);
		this.modeIndex = 0;

		//this.title = Engine.GetGUIObjectByName(`${PREFIX}Title`);
		this.tabButtons = Engine.GetGUIObjectByName(`${PREFIX}TabButtons`);
		this.tabs = this.tabButtons.children.map((tab, index) => new TrainerButton(tab, index, this, true));

		this.rowsContainer = Engine.GetGUIObjectByName(`${PREFIX}Rows`);
		this.rows = this.rowsContainer.children.map((row, index) => new ProGUIRow(row, index, index));

		this.tabButtons.size = "100%-47 38 100% 361";

		this.rowsContainer.size = (g_IsObserver || !this.configTrainerButtonIsEnabled) ? "0 39 100% 100%" : "0 39 100%-50 100%";

		this.forceRender = forceRender;

		// initate tabs
		this.tabs.forEach(tab => tab.update(this.modeIndex));
		this.setTopOffset(36);
		g_OverlayCounterManager.registerResizeHandler(this.setTopOffset.bind(this));
		registerConfigChangeHandler(this.onConfigChange.bind(this));
    }
    onConfigChange(changes) {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("moderngui.trainer")))
            this.getSettings();
    }
    getSettings() {
		this.trainerEnabled = Engine.ConfigDB_GetValue("user", "moderngui.trainer.enable") == "true"; //Actually is the setting for all "ecoPanels
		this.hideTabsWhenOff = Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.hideTabsWhenOff`) == "true";
		this.trainerIncrementSpeed = Math.round(parseFloat(Engine.ConfigDB_GetValue("user", "moderngui.trainer.increment")));
		this.toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "moderngui.trainer.color") + ".dds"
		this.toggleResetToZero = Engine.ConfigDB_GetValue("user", "moderngui.trainer.toogleResetToZero") == "true";
		this.configTrainerButtonIsEnabled = Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.0`) == "true";
	}
	setTopOffset(offset) {
		this.root.size = `100%-199 ${155 + offset} 100% 500`;
	}
	updateMode(modeIndex) {
		this.tabs.forEach(tab => tab.update(modeIndex));
		this.forceRender();
	}
	toggleMode(modeIndex) {
		const mode = Trainer.Modes[modeIndex];
		if (!mode) return;
		const toggledIcon = this.toggledIcon;
		let isModeToggled = (Engine.GetGUIObjectByName(`StatsModesTabButton[${modeIndex}]_Background`).sprite == toggledIcon) ? true : false;
		if (!isModeToggled) {
			this.toggledModes.push({

				"name": mode.type,
				"value": this.tabs[modeIndex].toProduce.caption * 1,
				"count": 0
			});
		}
		else
			this.toggledModes = this.toggledModes.filter(m => m.name != mode.type);
		this.updateMode(modeIndex);
		if (this.toggleResetToZero)
			this.decreaseMode(`StatsModesTabButton${modeIndex}`, true);
	}
	isInRange(num) {
		if (num >= 0 && num <= 100)
			return true;
		return false;
	}
	decreaseMode(name, setToZero) {
		let increment = this.trainerIncrementSpeed;
		let idString = name.replace(/\D/g, '');
		let modeId = Number.parseInt(idString);
		let mode = this.tabs[modeId];

		if (mode.isChecked == false || !this.toggleResetToZero) {
			// Calculate the new caption for the current mode
			let newCaption = mode.toProduce.caption * 1 - increment;
			while (!this.isInRange(newCaption) && increment >= 1) {
				increment--;
				newCaption = mode.toProduce.caption * 1 - increment;
			}
			// Calculate the total caption of all modes except the current one
			let otherModesCaptionTotal = this.tabs.reduce((total, tab, i) => {
				if (i !== modeId) {
					return total + tab.toProduce.caption * 1;
				}
				return total;
			}, 0);
			if ((!this.toggleResetToZero || otherModesCaptionTotal + newCaption < 0) && mode.isChecked == false) {
				// Scale down the captions of other modes proportionally
				let scalingFactor = increment / otherModesCaptionTotal;
				let totalincrement = 0;
				if (otherModesCaptionTotal > 0) {

					for (let i = 0; i < this.tabs.length; i++) {
						if (i !== modeId) {
							let otherMode = this.tabs[i];
							let otherModeCaption = otherMode.toProduce.caption * 1;
							let scaledCaption = Math.ceil(Math.max(otherModeCaption * 1 * (1 + scalingFactor), 0));
							if (this.isInRange(scaledCaption)) {
								totalincrement += otherModeCaption - scaledCaption;
								otherMode.toProduce.caption = scaledCaption;
							}
						}
					}
					if (totalincrement > 0) {
						newCaption = mode.toProduce.caption * 1 + totalincrement;
					}
					mode.toProduce.caption = newCaption;
				}
				if (setToZero)
					mode.toProduce.caption = 0;
				else {
					mode.toProduce.caption = newCaption;
					this.updateMode();
					let totalCaption = this.tabs.reduce((totalCap, tab) => totalCap + tab.toProduce.caption * 1, 0);
					if (totalCaption != 100) {
						let reScalingFactor = 1 / (totalCaption / 100);
						for (let i = 0; i < this.tabs.length; i++) {
							let otherMode = this.tabs[i];
							let otherModeCaption = otherMode.toProduce.caption * 1;
							let scaledCaption = Math.round(otherModeCaption * reScalingFactor);
							if (this.isInRange(scaledCaption))
								otherMode.toProduce.caption = scaledCaption;
						}
					}
				}
			}
			else
				mode.toProduce.caption = mode.toProduce.caption * 1 - increment;
			// Update the caption of the current mode and call the updateMode method
			this.updateMode();
		}
	}
	increaseMode(name) {

		let increment = this.trainerIncrementSpeed * 2;
		let idString = name.replace(/\D/g, '');
		let modeId = Number.parseInt(idString);
		let mode = this.tabs[modeId];
		if (mode.isChecked == false || !this.toggleResetToZero) {
			// Calculate the new caption for the current mode
			let newCaption = mode.toProduce.caption * 1 + increment
			// Update the caption of the current mode and call the updateMode method

			while (!this.isInRange(newCaption) && increment >= 1) {
				increment--;
				newCaption = mode.toProduce.caption * 1 + increment;
			}

			// Calculate the total caption of all modes except the current one
			let otherModesCaptionTotal = this.tabs.reduce((total, tab, i) => {
				if (i !== modeId) {
					return total + tab.toProduce.caption * 1;
				}
				return total;
			}, 0);
			if ((!this.toggleResetToZero || otherModesCaptionTotal + newCaption > 100)) {
				// Scale up the captions of other modes proportionally
				let scalingFactor = (increment) / otherModesCaptionTotal;
				let totalincrement = 0;
				if (otherModesCaptionTotal > 0) {

					for (let i = 0; i < this.tabs.length; i++) {
						if (i !== modeId) {
							let otherMode = this.tabs[i];
							let otherModeCaption = otherMode.toProduce.caption * 1;
							let scaledCaption = Math.floor(otherModeCaption * 1 * (1 - scalingFactor));
							if (this.isInRange(scaledCaption)) {
								totalincrement += otherModeCaption - scaledCaption;
								otherMode.toProduce.caption = scaledCaption;
							}
						}
					}
					if (totalincrement > 0) {
						newCaption = mode.toProduce.caption * 1 + totalincrement;
					}

				}

				mode.toProduce.caption = newCaption;
				this.updateMode();
				let totalCaption = this.tabs.reduce((totalCap, tab) => totalCap + tab.toProduce.caption * 1, 0);
				if (totalCaption != 100) {
					let reScalingFactor = 1 / (totalCaption / 100);
					for (let i = 0; i < this.tabs.length; i++) {
						let otherMode = this.tabs[i];
						let otherModeCaption = otherMode.toProduce.caption * 1;
						let scaledCaption = Math.round(otherModeCaption * reScalingFactor);
						if (this.isInRange(scaledCaption))
							otherMode.toProduce.caption = scaledCaption;
					}
				}
			}
			else
				mode.toProduce.caption = mode.toProduce.caption * 1 + increment;
			// Update the caption of the current mode and call the updateMode method
			this.updateMode();
		}
	}
	update(playersStates) {
		if (g_ViewedPlayer == -1)
			this.rowsContainer.hidden = true;
		else
			this.rowsContainer.hidden = false;

		this.configTrainerButtonIsEnabled = Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.0`) == "true";
		this.rowsContainer.size = (!this.trainerEnabled || g_IsObserver || (!this.configTrainerButtonIsEnabled && this.hideTabsWhenOff)) ? "0 39 100% 100%" : "0 39 100%-50 100%";

		if (!Trainer.Modes[this.modeIndex])
			Trainer.Modes[this.modeIndex] = {};
		if (playersStates && Trainer.Modes[this.modeIndex]) {
			this.rows.forEach((row, i) => row.update(playersStates[0], Trainer.Modes[this.modeIndex].type, i));
		}

		this.tabButtons.hidden = (!this.trainerEnabled || g_IsObserver || (!this.configTrainerButtonIsEnabled && this.hideTabsWhenOff)) ? true : false;
		if (!this.trainerEnabled)
			return;
		if(g_IsObserver) return;
		this.playersStates = g_stats.playersStates
		this.userState = g_stats.userState;
		this.trainers = this.getPlayerEntities();
		this.trainableUnits = this.getPlayerTrainableUnits();
		// Store the initial order of the tabs
		if (this.initialTabOrder.length === 0) {
			this.initialTabOrder = Trainer.Modes.map(mode => mode.type);
		}

		// Store the captions and toggle properties of the current tabs before deletion
		const tabState = this.tabs.map((tab, index) => ({
			caption: tab.toProduce.caption,
			isChecked: tab.isChecked
		}));
		// Iterate through Trainer.Modes and remove any elements not in trainableUnits anymore
		for (let i = Trainer.Modes.length - 1; i >= 0; i--) {
			const mode = Trainer.Modes[i];
			if (!this.trainableUnits.includes(mode.type)) {
				this.needToRefreshTabs = true;
				// Remove the mode from Trainer.Modes
				Trainer.Modes.splice(i, 1);

				// Remove all the tabs starting from this index
				if (!g_IsObserver) {
					this.removeTabsFromIndex(i, tabState);  // Pass tabState to keep track of properties
					break;  // Exit the loop after removing, since we are refreshing all tabs
				}
			}
		}
		// After removing, ensure all missing elements from trainableUnits are re-added
		this.addMissingTabs(tabState);
		// Now refresh the tabs to re-align the UI and restore captions and toggle properties
		if (this.needToRefreshTabs)
			this.refreshTabs(tabState);
	}
	// New function to remove all tabs from a specific index onward
	removeTabsFromIndex(index, tabState) {
		// Loop through the tabs from the specified index and remove each one
		for (let i = this.tabs.length - 1; i >= index; i--) {
			// Optionally animate the removal
			/*if (typeof animate === "function") {
				let animationSettings = {
					"duration": 400,
					"size": "50 150% 0 150%",
					"onComplete": GUIObject => GUIObject.hidden = true
				};
				animate(this.tabButtons.children[i]).add(animationSettings);
			} else {*/
			// Remove the child element from the parent container
			this.tabButtons.children[i].hidden = true;
			//}
			// Remove the tab from the tabs array
			this.tabs.splice(i, 1);
		}

		// Shift the state properties for remaining tabs after removal
		for (let i = index; i < tabState.length; i++) {
			if (i + 1 < tabState.length) {
				tabState[i] = tabState[i + 1];  // Shift properties to the left
			} else {
				delete tabState[i];  // Remove the last item that is no longer needed
			}
		}
	}

	// Function to re-add any missing tabs after deletion
	addMissingTabs(tabState) {
		for (let unit of this.trainableUnits) {
			const index = Trainer.Modes.findIndex(m => m.type == unit);

			// If a unit is missing in Trainer.Modes, add it
			if (index === -1) {
				this.needToRefreshTabs = true;
				const template = GetTemplateData(unit);
				const newMode = {
					"type": unit,
					"icon": template.icon,
					"title": template.name.generic
				};

				// Find the correct position for the new mode
				const initialIndex = this.initialTabOrder.indexOf(unit);
				if (initialIndex !== -1) {
					Trainer.Modes.splice(initialIndex, 0, newMode);
				} else {
					Trainer.Modes.push(newMode);
					this.initialTabOrder.push(unit);
				}

				let newIndex = Trainer.Modes.indexOf(newMode);

				// Add the corresponding tab
				if (!g_IsObserver) {
					/*if (typeof animate === "function") {
						let animationSettings = {
							"duration": 250,
							"size": "0 100% 0 100%"
						};
						animate(this.tabButtons.children[newIndex]).add(animationSettings);
					}*/
					this.tabs.push(new TrainerButton(this.tabButtons.children[newIndex], newIndex, this));
				}

				// Now shift the state properties for all tabs after the newly added tab
				for (let i = newIndex + 1; i < this.tabs.length; i++) {
					// Shift properties to the left for the succeeding tabs
					this.tabs[i].toProduce.caption = tabState[i - 1].caption;
					this.tabs[i].isChecked = tabState[i - 1].isChecked;
					// Optionally, you can modify other properties here as well
				}

				// Update the tabState for the new tab
				tabState.splice(newIndex, 0, {
					caption: 0,
					isChecked: false // Default value or you can set a different condition for checked state
				});
			}
		}
	}

	// Function to refresh all tabs and restore their captions and toggle properties
	refreshTabs(tabState) {
		let toToggleModes = new Array();
		//warn("tab refreshed");
		// Redefine this.tabs after re-arranging the children
		this.tabs = this.tabButtons.children.map((tab, index) => {
			// Create a new tab instance
			let newTab = new TrainerButton(tab, index, this, false);

			// Restore the caption and isChecked property from the saved state if available
			if (tabState[index]) {
				this.needToRefreshTabs = false;
				newTab.toProduce.caption = tabState[index].caption;

				// Restore the toggle state and handle background sprite updates
				if (tabState[index].isChecked && newTab.bg.sprite != this.toggledIcon) {
					toToggleModes.push(index);
				}
			} else {
				// If there's no state for this tab, try to find the state for the mode at this index
				const mode = Trainer.Modes[index];
				if (mode) {
					const modeIndex = this.initialTabOrder.indexOf(mode.type);
					if (modeIndex !== -1 && tabState[modeIndex]) {
						newTab.toProduce.caption = tabState[modeIndex].caption;

						// Restore the toggle state and handle background sprite updates
						if (tabState[modeIndex].isChecked && newTab.bg.sprite != this.toggledIcon) {
							toToggleModes.push(index);
						}
					}
				}
			}

			return newTab;
		});

		// Toggle modes for tabs that were checked before
		for (let index of toToggleModes) {
			this.toggleMode(index);
		}
	}


	getPlayerEntities() {

		let trainers = [];
		Engine.ProfileStart("Trainer:GetPlayerEntities");
		this.userState?.productionBuildings?.forEach(building => {
			if (building.mode == "Trainer") {
				trainers.push(...building.entity);
			}
		});
		Engine.ProfileStop();
		return trainers;
	}
	getPlayerTrainableUnits() {
		let availableUnits = new Array();
		let trainableUnits = new Array();
		for (let trainerID of this.trainers) {
			let state = GetEntityState(trainerID);
			if (state?.trainer && this.userState) {
				availableUnits = [...state.trainer.entities];
			}
			for (let availableUnit of availableUnits) {
				const template = availableUnit;
				if (GetTemplateData(template).visibleIdentityClasses.indexOf("Hero") != -1)
					continue;
				if (GetTemplateData(template).requirements) {

					const technologyEnabled =
						Engine.GuiInterfaceCall("AreRequirementsMet", {
							"requirements": GetTemplateData(availableUnit).requirements,
							"player": this.userState.index
						});
					if (!technologyEnabled)
						continue;
				}
				if (trainableUnits.indexOf(availableUnit) === -1)
					trainableUnits.push(availableUnit);
			}
		}
		return trainableUnits;
	}
	getPlayersStates() {
		return Engine.GuiInterfaceCall("moderngui_GetOverlay", {
			g_IsObserver, g_ViewedPlayer, g_LastTickTime
		}).players ?? [];
	}
	hide() {
		this.root.hidden = true;
		this.root.size = "-500 -500 0 0";
	}
	display() {
		this.root.hidden = false;
		this.setTopOffset(36);
	}
}