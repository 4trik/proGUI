class EcoPanel {
    constructor(forceRender, prefix, modes) {
        this.forceRender = forceRender;
        this.modes = modes;
        this.areButtonsHidden = true;
        this.isPaused = false;
        const PREFIX = "ControlPanel";
        this.panelButtonPrefix = `${PREFIX}TabButton`;
    }

    onConfigChange(changes) {
        const changesList = [...changes];
        if (changesList.some(x => x.startsWith("moderngui.trainer"))) {
            this.getSettings();
            this.setMenuAspect();
        }
    }
    hide() {
        if (this.panelButtons)
            this.panelButtons.hidden = true;
    }
    show() {
        if (this.panelButtons)
            this.panelButtons.hidden = false;
    }
    getSettings() {
        this.stackedSubMenus = Engine.ConfigDB_GetValue("user", `moderngui.trainer.controlPanel.showAllMenus`) == "true";
    }

    setMenuAspect() {
        if (this.stackedSubMenus) {
            this.areButtonsHidden = false;
            this.buttons.forEach(button => button.hide(this.areButtonsHidden));
        }
    }

    toggleMode(modeIndex, endPause) {
        if (!g_IsObserver) {
            const mode = this.modes[modeIndex];
            if (!mode) return;
            let isModeToggled = Engine.GetGUIObjectByName(`${this.panelButtonsName}[${modeIndex}]`).checked;
            this.updatePauseState(mode, isModeToggled, endPause);
            this.buttons.forEach(button => button.update(modeIndex));
            this.forceRender();
        }
    }

    updatePauseState(mode, isModeToggled, endPause) {
        if (endPause) {
            this.isPaused = false;
            Engine.GetGUIObjectByName(`${this.panelButtonPrefix}[8]`).checked = false; // Assuming Phase is at index 8
        } else {
            if (mode.type === "Phase") {
                this.isPaused = !isModeToggled;
            }
        }
    }

    hideShowButtons(forceHide) {
        this.areButtonsHidden = forceHide === true ? true : forceHide === false ? false : !this.areButtonsHidden;
        this.buttons.forEach(button => button.hide(this.areButtonsHidden));
    }
}