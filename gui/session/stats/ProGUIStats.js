class ProGUIStats extends ModernGUIStats {
	constructor(playerViewControl) {
		super(playerViewControl);
		this.initStats("Pro");
		if (Engine.ConfigDB_GetValue("user", "moderngui.trainer.enable") == "true") {
			this.initEcoPanels();
		}
	}
	initEcoPanels() {
		this.controlPanel = new EcoPanelMasterPanel(() => this.shouldForceRender);
		this.reservePanel = new EcoPanelTrainerReservePanel(() => this.shouldForceRender);
		this.techPanel = new EcoPanelTechPanel(() => this.shouldForceRender);
		this.ecoTechPanel = new EcoPanelEcoTechPanel(() => this.shouldForceRender);
		this.tributePanel = new EcoPanelTributePanel(() => this.shouldForceRender);
		this.ecoPanels = [
			this.controlPanel,
			this.reservePanel,
			this.techPanel,
			this.ecoTechPanel,
			this.tributePanel
		];
		this.ecoPanels.forEach(panel => {
			if (panel) {
				panel.show();
			}
		});
	}
	update() {
		super.update();
		if (Engine.ConfigDB_GetValue("user", "moderngui.trainer.enable") == "true" && !this.ecoPanels)
			this.initEcoPanels();
		else if ((Engine.ConfigDB_GetValue("user", "moderngui.trainer.enable") != "true" || g_IsObserver) && this.ecoPanels){
			this.ecoPanels.forEach(panel => {
				if (panel) {
					panel.hide();
				}
				this.ecoPanels = null;
			});
		}
	}
}
