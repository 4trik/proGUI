const g_BoonGUIResTypes = ["food", "wood", "stone", "metal"];

/**
 * @param {number} index
 * @param {number} height
 */
function ProGUIGetRowSize(index, height, offset) {
	if (!offset)
		offset = 0;
	const y1 = height * index + offset;
	const y2 = height * (index + 1) + offset;
	return `0 ${y1} 100% ${y2}`;
}
function BoonGUIGetRowSize(index, height) {
	const y1 = height * index;
	const y2 = height * (index + 1);
	return `0 ${y1} 100% ${y2}`;
}

/**
 * @param {number} index
 * @param {number} width
 * @param {boolean} rtl
 */
function ProGUIGetColSize(index, width, rtl = false) {
	const x1 = width * index;
	const x2 = width * (index + 1);
	return rtl ? `100%-${x2} 0 100%-${x1} 100%` : `${x1} 0 ${x2} 100%`;
}
function BoonGUIGetColSize(index, width, rtl = false) {
	const x1 = width * index;
	const x2 = width * (index + 1);
	return rtl ? `100%-${x2} 0 100%-${x1} 100%` : `${x1} 0 ${x2} 100%`;
}

/**
 * When a fully constructed civic center (CC) exits, the camera is focused at it. If no CC is present, a sound is played.
 * @param  {boolean} move
 * @param  {Object} state
 */
function focusCC(move, state) {
	if (state == null || state.civCentres.length <= 0) {
		Engine.PlayUISound("audio/interface/alarm/alarm_invalid_building_placement_01.ogg", false);
		return;
	}
	if (!Engine.HotkeyIsPressed("selection.add"))
		g_Selection.reset();

	g_Selection.addList(state.civCentres);

	moveToNextEntity(state.civCentres, move)
}
/**
 *
 * @param {string} civName
 */
async function openStructTree(civName) {
	await closeOpenDialogs();
	await g_PauseControl.implicitPause();

	const data = await Engine.PushGuiPage(
		BoonGUIStatsTopPanelRow.prototype.civInfo.page,
		{
			"civ": civName || g_Players[Math.max(g_ViewedPlayer, 1)].civ,
		}
	);

	await storeCivInfoPage(data);
	await resumeGame();
}

/**
 *
 * @param {object} data
 */
async function storeCivInfoPage(data) {
	if (data.nextPage) {
		const nextData = await Engine.PushGuiPage(data.nextPage, { "civ": data.args.civ });
		await storeCivInfoPage(nextData);
	} else {
		BoonGUIStatsTopPanelRow.prototype.civInfo = data;
	}
}


/**
 * @param {Object} objectPlayer
 * @param {string} playerName
 * @param {Object} objectRating
 * @param {number} rating
 * @param {number} smallSafetyMargin
 * @returns abbreviated player name with an elipsses if too long
 */
function limitPlayerName(objectPlayer, playerName, objectRating, rating, smallSafetyMargin = 8) {
	// check if a cached version is already available
	if (BoonGUIStatsTopPanelRow.prototype.abbreviatedPlayerNames[playerName])
		return BoonGUIStatsTopPanelRow.prototype.abbreviatedPlayerNames[playerName];

	const { "right": objectPlayertRight, "left": objectPlayertLeft } = objectPlayer.getComputedSize();
	let widthBox = objectPlayertRight - objectPlayertLeft;
	widthBox -= smallSafetyMargin;
	if (rating) {
		const { "right": objectRatingtRight, "left": objectRatingtLeft } = objectRating.getComputedSize();
		widthBox -= (objectRatingtRight - objectRatingtLeft);
	}
	let abbreviatedName = playerName;
	let playerNameLength = Engine.GetTextWidth(objectPlayer.font, abbreviatedName);

	for (let i = 1; playerNameLength > widthBox; i++) {
		abbreviatedName = `${playerName.slice(0, -i)}…`;
		playerNameLength = Engine.GetTextWidth(objectPlayer.font, abbreviatedName);
	}

	BoonGUIStatsTopPanelRow.prototype.abbreviatedPlayerNames[playerName] = abbreviatedName;
	return BoonGUIStatsTopPanelRow.prototype.abbreviatedPlayerNames[playerName];

}
/**
 * Moves the camera to the next entity of a specific class.
 * @param {string} className - The class name to filter for.
 * @param {number} playerID - Player ID of the entity owner.
 */
function moveToNextEntityOfClass(className, playerID) {
	// Get all player entities
	let entities = Engine.GuiInterfaceCall("GetPlayerEntities", {"playerID" : playerID});
	// Filter entities by the specified class
	const classEntities = entities.filter(entity => hasClass(GetEntityState(entity), className));
	addEntitiesToSelection(classEntities, playerID)
	// Move the camera to the next entity in the list
	moveToNextEntity(classEntities, true);
}
var currentIndex = 0;
/**
 * Moves the camera to the next entity in the provided list.
 * @param {any[]} entities
 * @param {boolean} move
 */
function moveToNextEntity(entities, move) {
	if (move) {
		if (currentIndex === undefined || currentIndex >= entities.length - 1) {
			currentIndex = 0;
		} else {
			currentIndex++;
		}

		const entState = GetEntityState(entities[currentIndex]);
		if (entState?.position)
			Engine.CameraMoveTo(entState.position.x, entState.position.z);
	}
}
/**
 * Add Entities to Selection.
 * @param {array} entities - Array of entities to add to selection.
 * @param {number} playerID - Player ID of the entity owner.
 */
function addEntitiesToSelection(entities, playerID) {
	if (!Engine.HotkeyIsPressed("selection.add"))
		g_Selection.reset();
	if (playerID != g_ViewedPlayer && g_ViewedPlayer != -1){
		g_Selection.reset();
		g_Selection.addList([entities[currentIndex]]);
	}
	else
		g_Selection.addList(entities);
}

const normalizeResourceCount = value =>
	// regex to avoid trailing zeros
	value >= 1e6 ?
		Math.floor(value / 1e6) + setStringTags("M", { "font": "sans-stroke-12" }) :
		value >= 1e5 ?
			Math.floor(value / 1e3) + setStringTags("k", { "font": "sans-stroke-12" }) :
			value >= 1e3 ?
				(value / 1e3).toFixed(1).replace(/\.0$/, "") + setStringTags("k", { "font": "sans-stroke-12" }) :
				// for rounding number to its tenth
				Math.floor(value / 10) * 10;

const normalizeValue = value =>
	value >= 1e4 ?
		Math.floor(value / 1e3) + setStringTags("k", { "font": "mono-10" }) :
		value >= 1e3 ?
			(value / 1e3).toFixed(1).replace(/\.0$/, "") + setStringTags("k", { "font": "mono-10" }) :
			value;
