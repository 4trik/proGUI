// Called by GUI when user clicks training button
function addTrainingToQueue(selection, trainEntType, playerState) {
    // Get all buildings that can train the specified entity type
    let appropriateBuildings = getBuildingsWhichCanTrainEntity(selection, trainEntType);

    // Filter to include only idle buildings
    let idleBuildings = appropriateBuildings.filter(building => {
        const state = GetEntityState(building);
        return state?.production?.queue.length === 0; // Only keep idle buildings
    });

    // If there are no idle buildings, fall back to using all appropriate buildings
    if (idleBuildings.length === 0) {
        idleBuildings = appropriateBuildings;
    }
    let buildingsToUse = appropriateBuildings;
    if (Engine.HotkeyIsPressed("moderngui.training.addOnlyTo1Building.hold")) {
        // Find the building with the least total time remaining that is capable of training
        let leastBusyBuilding = null;
        let leastTotalTimeRemaining = Infinity;

        for (const entid of appropriateBuildings) {
            const state = GetEntityState(entid);
            const buildingState = GetEntityState(state.id);
            const productionQueue = buildingState?.production?.queue;

            // Check if the building has a trainer and entities
            let canTrain = true; // Flag to check if all requirements are met
            if (state?.trainer && state.trainer.entities) {

                // Check each trainable unit for requirements
                for (const trainableUnit of state.trainer.entities) {
                    const requirements = GetTemplateData(trainableUnit).requirements;

                    // Check if technology requirements are met
                    if (requirements) {
                        const technologyEnabled = Engine.GuiInterfaceCall("AreRequirementsMet", {
                            requirements,
                            player: g_ViewedPlayer
                        });
                        if (!technologyEnabled) {
                            canTrain = false;
                            continue;
                        }
                        else {
                            canTrain = true;
                            break;
                        }
                    }
                }

                // Only proceed if the building can train
                if (canTrain) {
                    let totalTimeRemaining = 0;

                    // Sum up the time remaining for all items in the production queue
                    if (productionQueue && productionQueue.length > 0) {
                        for (const item of productionQueue) {
                            totalTimeRemaining += item.timeRemaining;
                        }

                        // Compare total time remaining with the least found so far
                        if (totalTimeRemaining < leastTotalTimeRemaining) {
                            leastTotalTimeRemaining = totalTimeRemaining;
                            leastBusyBuilding = state.id;
                        }
                    } else {
                        // If there is no production queue, treat it as having 0 time remaining
                        if (0 < leastTotalTimeRemaining) {
                            leastTotalTimeRemaining = 0;
                            leastBusyBuilding = state.id;
                        }
                    }
                }
            }
        }
        // If a least busy building was found, use it
        buildingsToUse = leastBusyBuilding ? [leastBusyBuilding] : [];
    } else {
        // Use only idle buildings if available
        buildingsToUse = idleBuildings;
    }

    let canBeAddedCount = getEntityLimitAndCount(playerState, trainEntType).canBeAddedCount;

    let decrement = Engine.HotkeyIsPressed("selection.remove");
    let template;
    if (!decrement)
        template = GetTemplateData(trainEntType);

    // Batch training only possible if we can train at least 2 units.
    if (Engine.HotkeyIsPressed("session.batchtrain") && (canBeAddedCount == undefined || canBeAddedCount > 1)) {
        if (inputState == INPUT_BATCHTRAINING) {
            // Check if we are training in the same structure(s) as the last batch.
            if (g_BatchTrainingEntities.length == buildingsToUse.length &&
                g_BatchTrainingEntities.every((ent, i) => ent == buildingsToUse[i]) &&
                g_BatchTrainingType == trainEntType) {
                if (decrement) {
                    --g_NumberOfBatches;
                    if (g_NumberOfBatches <= 0)
                        inputState = INPUT_NORMAL;
                } else if (canBeAddedCount == undefined ||
                    canBeAddedCount > g_NumberOfBatches * getBatchTrainingSize() * buildingsToUse.length) {
                    if (Engine.GuiInterfaceCall("GetNeededResources", {
                        "cost": multiplyEntityCosts(template, (g_NumberOfBatches + 1) * getBatchTrainingSize())
                    }))
                        return;

                    ++g_NumberOfBatches;
                }
                g_BatchTrainingEntityAllowedCount = canBeAddedCount;
                return;
            } else if (!decrement)
                flushTrainingBatch();
        }

        if (decrement || Engine.GuiInterfaceCall("GetNeededResources", {
            "cost": multiplyEntityCosts(template, getBatchTrainingSize())
        }))
            return;

        inputState = INPUT_BATCHTRAINING;
        g_BatchTrainingEntities = buildingsToUse;
        g_BatchTrainingType = trainEntType;
        g_BatchTrainingEntityAllowedCount = canBeAddedCount;
        g_NumberOfBatches = 1;
    } else {
        let buildingsForTraining = buildingsToUse;
        if (canBeAddedCount !== undefined)
            buildingsForTraining = buildingsForTraining.slice(0, canBeAddedCount);
        Engine.PostNetworkCommand({
            "type": "train",
            "template": trainEntType,
            "count": 1,
            "entities": buildingsForTraining,
            "pushFront": Engine.HotkeyIsPressed("session.pushorderfront")
        });
    }
}