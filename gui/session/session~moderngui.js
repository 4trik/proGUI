var g_QuickStart;
var g_PanelScripts;
var g_stockfish;
var g_GUI = Engine.ConfigDB_GetValue("user", `moderngui.ProOrBoon`);

const runQuickStart = Engine.ConfigDB_GetValue("user", "moderngui.quickStart.enabled") == "true" ? true : false;
const runPanelScripts = Engine.ConfigDB_GetValue("user", "moderngui.trainer.enable") == "true" ? true : false;
const runStockfish = Engine.ConfigDB_GetValue("user", "moderngui.stockfish.enable") == "true" ? true : false;

// Wrapper for the "init" function
function init_Wrapper_ModernGUI(target, args) {
	// Run original function
	target(...args);

	// Run additional code
	if (runPanelScripts)
		g_PanelScripts = new PanelScripts();
	if (g_InitAttributes?.settings?.Nomad)
		return;
	if (runQuickStart)
		g_QuickStart = new QuickStart();

}

// Handler for the "init" function
const init_Handler_ModernGUI = {
	apply: function (target, thisArg, args) {
		return init_Wrapper_ModernGUI(target, args);
	}
};

// Proxy the "init" function
init = new Proxy(init, init_Handler_ModernGUI);


let g_stats;

// keep in sync with GuiInterface~moderngui.js
// Define worker types for Boon GUI
var g_boonGUI_WorkerTypes = ["FemaleCitizen", "Trader", "FishingBoat", "CitizenSoldier+!Mercenary"];

if (typeof autociv_patchApplyN === "function") {
	autociv_patchApplyN("init", function (target, that, args) {
		const result = target.apply(that, args);
		initializeStats();
		if (runStockfish) {
			g_stockfish = new stockfish();
		}
		return result;
	});
}

function endHome() {
	const replayDirectory = Engine.GetCurrentReplayDirectory();
	Engine.EndGame();

	if (!g_IsReplay) {
		Engine.AddReplayToCache(replayDirectory);
	}

	if (g_IsNetworked && Engine.HasXmppClient()) {
		Engine.SendUnregisterGame();
		Engine.SwitchGuiPage("page_lobby.xml");
	} else {
		Engine.SwitchGuiPage("page_pregame.xml");
	}
}

/**
 * Initialize stats based on the current GUI type.
 */
function initializeStats() {
	if (!g_Settings) return;

	if (!g_stats) {
		setStatsBasedOnGUI();
	}
}

/**
 * Set stats based on the current GUI type.
 */
function setStatsBasedOnGUI() {
	const guiType = g_GUI || Engine.ConfigDB_GetValue("user", `moderngui.ProOrBoon`);
	switch (guiType) {
		case "Boon":
			g_stats = new BoonGUIStats(g_PlayerViewControl);
			break;
		case "Pro":
		default:
			g_stats = new ProGUIStats(g_PlayerViewControl);
			break;
	}
	g_stats.display();
}

/**
 * Update GUI stats if the GUI type has changed.
 */
function updateStatsIfChanged() {
	const currentGUI = g_IsObserver ? Engine.ConfigDB_GetValue("user", `moderngui.ProOrBoonObs`) : Engine.ConfigDB_GetValue("user", `moderngui.ProOrBoon`);

	if (g_GUI !== currentGUI) {
		g_GUI = currentGUI;
		g_stats.delete();
		setStatsBasedOnGUI();
	}
}

/**
 * Called every frame.
 */
function onTick() {
	Engine.ProfileStart("ModernGUI:tick");
	initializeStats();
	updateStatsIfChanged();

	const now = Date.now();
	const tickLength = now - g_LastTickTime;
	g_LastTickTime = now;

	handleNetMessages();

	updateCursorAndTooltip();

	if (g_Selection.dirty) {

		g_Selection.dirty = false;
		// When selection changed, get the entityStates of new entities
		GetMultipleEntityStates(g_Selection.filter(entId => !g_EntityStates[entId]));

		for (const handler of g_EntitySelectionChangeHandlers)
			handler();

		updateGUIObjects();

		// Display rally points for selected structures.
		Engine.GuiInterfaceCall("DisplayRallyPoint", { "entities": g_Selection.toList() });
	}
	else {
		if (g_ShowAllStatusBars && now % g_StatusBarUpdate <= tickLength)
			recalculateStatusBarDisplay();

		Engine.GuiInterfaceCall("DisplayRallyPoint", { "entities": g_Selection.toList(), "watch": true });
	}

	updateTimers();
	Engine.GuiInterfaceCall("ClearRenamedEntities");
	const isPlayingCinemaPath = GetSimState().cinemaPlaying && !g_Disconnected;
	if (isPlayingCinemaPath)
		updateCinemaOverlay();

	if (runQuickStart && g_QuickStart != null && !g_QuickStart.isTooLate()) { //if (g_QuickStart) { //
		//QuickStart	
		quickStart();
	}
	else if (Engine.ConfigDB_GetValue("user", `moderngui.ProOrBoon`) == "Pro" && runPanelScripts) {
		//ProGUI PanelScripts
		if (!g_IsObserver && runPanelScripts) {
			if (!g_PanelScripts) {
				warn("ProGUI helpers failed to init retrying...");
				try {
					g_PanelScripts = new PanelScripts();

				}
				catch (error) { error("ProGUI helpers failed to init." + error); }
				finally { return }
			}
			g_PanelScripts.tickTasks();
			//StockFish
			if (!g_stockfish && runStockfish) {
				try {
					g_stockfish = new stockfish();
				}
				catch (error) { error("StockFish helpers failed to init." + error); }
				finally { return }
			}
			if (Engine.ConfigDB_GetValue("user", "moderngui.stockfish.enable") === "true")
				g_stockfish.tickTasks();
		}
	}
	Engine.ProfileStop();
}
function quickStart() {

	if (g_QuickStart.isTooLate() || g_QuickStart.isAborted()) {
		g_QuickStart = null;
		return;
	}

	if (g_QuickStart.isStartComplete()) {
		try {
			g_QuickStart.garrison(); //No init seq. of QuickStart
			g_QuickStart.trainCitizens();
			g_QuickStart.currentState = "BEFORE_UNLOAD";
		}
		catch (error) { g_QuickStart = null }
		finally { return }
	}

	if (g_QuickStart.isGarrisonComplete()) {
		try {
			g_QuickStart.unload();
			g_QuickStart.selectCivicCenter();
			g_QuickStart.currentState = "UNLOADING";
		}
		catch (error) { g_QuickStart = null }
		finally { return }
	}

	if (g_QuickStart.isUnloadComplete()) {
		try {
			g_QuickStart.selectCitizens();
			g_QuickStart.placeFoundation();
			g_QuickStart.setFinalRallyPoint();
			g_QuickStart = null;
		}
		catch (error) { g_QuickStart = null }
		finally { return }
	}
}
