class GameSetupPage_ModernGUI extends SetupWindowPages.GameSetupPage {

    constructor(setupWindow, isSavedGame)
    {
        super(setupWindow, isSavedGame);
        this.panelButtons["resetColorsButton"] = new ResetColorsButton(setupWindow);
    }
};
SetupWindowPages.GameSetupPage = GameSetupPage_ModernGUI;