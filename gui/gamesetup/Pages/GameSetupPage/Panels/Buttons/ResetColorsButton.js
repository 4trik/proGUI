class ResetColorsButton {
    constructor(setupWindow, isSavedGame) {
        this.gameSettingsController = setupWindow.controls.gameSettingsController;

        this.teamResetButton = Engine.GetGUIObjectByName("playerColorHeading");
        this.teamResetButton.tooltip = this.Tooltip;
        this.teamResetButton.onPress = this.onPress.bind(this);

        if (isSavedGame)
            this.teamResetButton.hidden = true;
        else
            g_GameSettings.map.watch(() => this.render(), ["type"]);
    }

    render() {
        this.teamResetButton.hidden = g_GameSettings.map.type === "scenario" || !g_IsController;
    }

    onPress() {
        for (let i = 0; i < g_GameSettings.playerCount.nbPlayers; ++i) {
            g_GameSettings.playerColor.values[i] = g_CustomColors.substituteByIndex(i);
            Engine.GetGUIObjectByName(`playerBackgroundColor[${i}]`).sprite = "color:" + rgbToGuiColor(g_GameSettings.playerColor.values[i], 100);
        }

        this.gameSettingsController.setNetworkInitAttributes();
    }
}

ResetColorsButton.prototype.Tooltip =
    translate("Reset all colors to the default.");
