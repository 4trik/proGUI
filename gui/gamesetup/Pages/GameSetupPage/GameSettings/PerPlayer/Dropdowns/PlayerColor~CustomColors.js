class PlayerColor_CustomColors extends PlayerSettingControls.PlayerColor {

    constructor(...args) {
        super(...args);
    }

    render(...args) {
        super.render(...args);
        if (!global.g_CustomColors)
            return;

        if (g_GameSettings.playerCount.nbPlayers < this.playerIndex + 1)
            return;

        // Change backgroud sprite
        this.playerBackgroundColor.sprite = "color:" + rgbToGuiColor(g_CustomColors.substitute(this.playerIndex, g_GameSettings.playerColor.get(this.playerIndex)), 100);

        // Change dropdown icons

        this.dropdown.list = this.values.map(x => coloredText(this.ColorIcon, rgbToGuiColor(g_CustomColors.substituteByColor(x))));
        if (Engine.ConfigDB_GetValue("user", "customcolors.AllowMoreColors") == "false")
            this.dropdown.tooltip = "[color=\"220 185 70\"] Colors have been [color=\"220 85 40\"]DISABLED[/color] in[color=\"60 135 190\"] OPTIONS [/color]: CustomColors. [/color] Other players can see normal colors."
    }

}

PlayerSettingControls.PlayerColor = PlayerColor_CustomColors;
