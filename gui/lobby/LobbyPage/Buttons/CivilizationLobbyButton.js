/**
 * This class manages the button that allows the player to go to see the civilizations info.
 */
class CivilizationLobbyButton {
	constructor() {
		this.civInfo = {
			"civ": "",
			"page": "page_civinfo.xml"
		};
		this.civilizationLobbyButton = Engine.GetGUIObjectByName("civilizationLobbyButton");
		this.civilizationLobbyButton.onPress = this.onPress.bind(this);
		this.civilizationLobbyButton.caption = translate("Civilization");

		Engine.SetGlobalHotkey("structree", "Press", this.openPage.bind(this, "page_structree.xml"));
		Engine.SetGlobalHotkey("civinfo", "Press", this.openPage.bind(this, "page_civinfo.xml"));
	}

	async onPress() {
		await this.openPage(this.civInfo.page);
	}

	async openPage(page) {
		const data = await Engine.PushGuiPage(
			page,
			{ "civ": this.civInfo.civ }
		);
		await this.storeCivInfoPage(data);
	}

	async storeCivInfoPage(data) {
		if (data.nextPage) {
			const nextData = await Engine.PushGuiPage(
				data.nextPage,
				{ "civ": data.civ }
			);
			await this.storeCivInfoPage(nextData);
		} else {
			this.civInfo = data; // Store the final data
		}
	}
}
