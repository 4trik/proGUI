init = new Proxy(init, {
    apply: function (target, thisArg, args) {
        target(...args);

        // Save default settings if they don't exist
        const settings = new CustomColorsSettings();
        settings.createDefaultSettingsIfNotExist();

        let options = Engine.ReadJSONFile("moddata/moderngui_options.json");
        options = Object.assign(options, Engine.ReadJSONFile("moddata/customcolors_option.json"));
        options[0].options.forEach((x, i) => {
            if (x.hasOwnProperty('val')) {
                x.label = coloredText(x.label, x.val) + "  →"
            }
        });
        g_Options = g_Options.concat(options);

        placeTabButtons(
            g_Options,
            false,
            g_TabButtonHeight,
            g_TabButtonDist,
            selectPanel,
            displayOptions
        );
    }
});
