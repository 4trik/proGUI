const buttonSettings = {
    "ecoPanels": {
      "setupEcoPanels": [
        { key: "moderngui.trainer.enable", value: "true" }
      ],
      "setupNoEcoPanels": [
        { key: "moderngui.trainer.enable", value: "false" }
      ],
    },
    "guiStyle": {
      "setupBoonguiStyle": [
        { key: "moderngui.ProOrBoon", value: "Boon" },
        { key: "moderngui.ProOrBoonObs", value: "Boon" },
        { key: "moderngui.teamBriefing", value: "false" },
        { key: "moderngui.topPanel.animateKD", value: "false" },
        { key: "moderngui.trainer.enable", value: "false" }
      ],
      "setupProguiStyle": [
        { key: "moderngui.ProOrBoon", value: "Pro" },
        { key: "moderngui.ProOrBoonObs", value: "Pro" },
        { key: "moderngui.teamBriefing", value: "false" },
        { key: "moderngui.topPanel.animateKD", value: "false" }
      ],
      "setupModernGUIStyle": [
        { key: "moderngui.ProOrBoon", value: "Pro" },
        { key: "moderngui.ProOrBoonObs", value: "Boon" },
        { key: "moderngui.teamBriefing", value: "true" },
        { key: "moderngui.topPanel.animateKD", value: "true" }
      ],
    },
  };
  
var modernGUIConfig = {
    needsToSave: false,
    needsToReloadHotkeys: false,
    set: function (key, value) {
        Engine.ConfigDB_CreateValue("user", key, value);
        this.needsToSave = true;
        this.needsToReloadHotkeys = this.needsToReloadHotkeys || key.startsWith("hotkey.");
    },
    get: function (key) { return Engine.ConfigDB_GetValue("user", key) },
    save: function () {
        if (this.needsToSave) Engine.ConfigDB_SaveChanges("user", "config/user.cfg");
        if (this.needsToReloadHotkeys) Engine.ReloadHotkeys();
    }
};
  
function init()
{
    
    Engine.GetGUIObjectByName("setupBoonguiStyle").caption = Engine.Translate("BoonGUI")
    Engine.GetGUIObjectByName("setupProguiStyle").caption = Engine.Translate("ProGUI")
    Engine.GetGUIObjectByName("setupModernGUIStyle").caption = Engine.Translate("ModernGUI")
    Engine.GetGUIObjectByName("setupEcoPanels").caption = Engine.Translate("EcoPanels")
    Engine.GetGUIObjectByName("setupNoEcoPanels").caption = Engine.Translate("No EcoPanels")
    Engine.GetGUIObjectByName("buttonClose").caption = Engine.Translate("Done! Close.")
    Engine.GetGUIObjectByName("title").caption = Engine.Translate("Quick Configuration")


    Engine.GetGUIObjectByName("setupEcoPanels").onPress = () => onButtonPress("setupEcoPanels");
    Engine.GetGUIObjectByName("setupNoEcoPanels").onPress = () => onButtonPress("setupNoEcoPanels");
    Engine.GetGUIObjectByName("setupBoonguiStyle").onPress = () => onButtonPress("setupBoonguiStyle");
    Engine.GetGUIObjectByName("setupProguiStyle").onPress = () => onButtonPress("setupProguiStyle");
    Engine.GetGUIObjectByName("setupModernGUIStyle").onPress = () => onButtonPress("setupModernGUIStyle");

     
    const markdown = Engine.ReadFile("moddata/moderngui_welcome.md")
    Engine.GetGUIObjectByName("text").caption = autociv_SimpleMarkup(markdown)
}

// Function to handle button presses
function onButtonPress(buttonName) {
    // Get the group for the pressed button
    const group = Object.keys(buttonSettings).find(key => buttonSettings[key][buttonName]);
  
    // Get the settings for the pressed button
    const settings = buttonSettings[group][buttonName];
  
    // Disable the pressed button
    Engine.GetGUIObjectByName(buttonName).enabled = false;
  
    // Enable the other buttons in the same group
    for (const otherButtonName in buttonSettings[group]) {
      if (otherButtonName !== buttonName) {
        Engine.GetGUIObjectByName(otherButtonName).enabled = true;
      }
    }
    // Handle exception for Boongui that can't support ecoPanels
    if (Engine.GetGUIObjectByName("setupBoonguiStyle").enabled == false && Engine.GetGUIObjectByName("setupEcoPanels").enabled == false){
        onButtonPress("setupNoEcoPanels");
        warn("EcoPanels only works with ProGUI or ModernGUI options!")
    }
    // Loop through the settings and add them
    for (const setting of settings) {
      modernGUIConfig.set(setting.key, setting.value);
      //warn(setting.key + " Set to: " + setting.value);
    }
    modernGUIConfig.set("moderngui.mainmenu.setupHelper", "false");
    // Save the changes
    modernGUIConfig.save();
  }